## 前期准备

### 学习 React

Cheui-React 基于 React.js（`v0.15.6+`）开发，如果你没有使用过 React，请先访问 [React 官网](https://facebook.github.io/react/index.html)学习。


## 获取 Cheui-React

### （1）包管理工具方式

- NPM私服安装

```shell
npm config set registry http://ires.58corp.com/repository/58npm             【代理源为淘宝源+58发布私包】
npm install cheui-react
```
- 组件调用方式

```js
import {Icon,Close,Toast} from 'cheui-react' 
....
render(){
        return(
                <div className="contain">
                    <Icon spin icon="cog" />
                    <Close alt spin icon onClick = {()=>{
                        Toast.showToast("click me",1);
                    }}/>
                </div>

            )
    }

```


### （2）获取源代码方式

Cheui-React 源代码托管在 GitLab 上，你可以点击下面的按钮获取。

- [源码下载](http://igit.58corp.com/fe_ershouche/cheui-react.git)
```shell
cd projeckt
git clone git@igit.58corp.com:fe_ershouche/cheui-react.git
//删掉cheui-react下的.babelrc和package.json文件，然后在项目中可以正常通过路径引用cheui-react
```

- 工程配置（webpack）

```js
//建议在webpack中配置resolve 别名配置
resolve: {
        extensions: ['', '.js', '.jsx'],
        alias: {
          'cheui-react':path.join(__dirname, './cheui-react/src'),
        }
    }
```


- 组件调用方式

```js
//配置alias别名
import {Icon,Close,Toast} from 'cheui-react' 

//无配置alias别名
import {Icon,Close,Toast} from './cheui-react/src' 
//cheui-react src是源码目录，lib是编译过后的文件
```

```js

render(){
        return(
                <div className="contain">
                    <Icon spin icon="cog" />
                    <Close alt spin icon onClick = {()=>{
                        Toast.showToast("click me",1);
                    }}/>
                </div>

            )
    }

```
*****
### 备注说明
- 第一种npm安装后文件源码在node_modules中，项目引用的组件为编译过后的，不可扩展  
- 第二种源码下载方式使用，可对代码进行个性化定制。如果为通用性扩展经过测试后可提交到cheui-react远程分支

### 常用错误
cheui-react 源码编译node-sass模块版本需要大于4.0.0 如果在使用过程中有sass编译报错 则需要本地项目升级node-sass模块
*****
