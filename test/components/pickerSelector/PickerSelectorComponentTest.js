/* eslint-env node, mocha */
/* global expect */
/* eslint no-console: 0 */
'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
// Uncomment the following lines to use the react test utilities
import TestUtils from 'react-addons-test-utils';
import createComponent from 'helpers/shallowRenderHelper';
import PickerSelectorComponent from 'components/pickerSelector/PickerSelector.js';

describe('PickerSelectorComponentTest with shallowRender', () => {
  let component;

  before(() => {
    let colorData = {
        paramname: 'cheshenyanse',
        title: '颜色',
        option: [
            { text: '黑', value: '1', paramname: 'cheshenyanse' },
            { text: '白', value: '2', paramname: 'cheshenyanse' },
            { text: '红', value: '3', paramname: 'cheshenyanse' },
            { text: '灰', value: '4', paramname: 'cheshenyanse' },
            { text: '黄', value: '5', paramname: 'cheshenyanse' },
            { text: '蓝', value: '6', paramname: 'cheshenyanse' },
            { text: '橙', value: '7', paramname: 'cheshenyanse' },
            { text: '紫', value: '8', paramname: 'cheshenyanse' },
            { text: '银', value: '9', paramname: 'cheshenyanse' }
        ]
    };
    let props = {
        data: colorData,
        title: '车身颜色',
        text: {"paraname":"cheshenyanse","text":"黄","value":"5"}
    }
    component = createComponent(PickerSelectorComponent, props);
  });

  it('should have its component name as default className', () => {
    expect(component.props.className).to.equal('pickerWrap');
  });

  it('its component title shoudle be "车身颜色"', () => {
      let title = component.props.children[1].props.children[0].props.children[1].props.children;
      expect(title).to.equal('车身颜色');
  });
});

describe('PickerSelectorComponentTest of type basicPicker', () => {
    let instance;
    let colorData = {
        paramname: 'cheshenyanse',
        title: '颜色',
        option: [
            { text: '黑', value: '1', paramname: 'cheshenyanse' },
            { text: '白', value: '2', paramname: 'cheshenyanse' },
            { text: '红', value: '3', paramname: 'cheshenyanse' },
            { text: '灰', value: '4', paramname: 'cheshenyanse' },
            { text: '黄', value: '5', paramname: 'cheshenyanse' },
            { text: '蓝', value: '6', paramname: 'cheshenyanse' },
            { text: '橙', value: '7', paramname: 'cheshenyanse' },
            { text: '紫', value: '8', paramname: 'cheshenyanse' },
            { text: '银', value: '9', paramname: 'cheshenyanse' }
        ]
    };
    let text = {"paraname":"cheshenyanse","text":"黄","value":"5"};

    before(() => {
        instance = TestUtils.renderIntoDocument(
            <PickerSelectorComponent data={colorData} text={text}></PickerSelectorComponent>
        );
    });
    it('should have its component type with one picker', () => {
        let wheelItems = TestUtils.scryRenderedDOMComponentsWithClass(instance, 'wheelItem');
        expect(wheelItems.length).to.equal(1);
    });
    
    it('Click the cancel button, the click function onClose is used', () => {
        var called = false;
        let onDone = () => {
            called = true;
        };
        let instance = TestUtils.renderIntoDocument(
            <PickerSelectorComponent data={colorData} text={text} onClose={onDone}></PickerSelectorComponent>
        );
        let cancelBtn = TestUtils.scryRenderedDOMComponentsWithClass(instance, 's0')[0];
        TestUtils.Simulate.click(cancelBtn);
        expect(called).to.equal(true);
    });
})

describe('PickerSelectorComponentTest of type timePicker', () => {
    let instance;

    before(() => {
        let timeData = {
            paramname: 'buytime',
            title: '购车时间',
            option: [
                { text: '2014年', value: '2014', paramname: 'buytime', option: [
                    {text: '1月', value: '515681', paramname: 'shangpaiyuefen'},
                    {text: '2月', value: '515682', paramname: 'shangpaiyuefen'},
                    {text: '3月', value: '515683', paramname: 'shangpaiyuefen'},
                    {text: '4月', value: '515684', paramname: 'shangpaiyuefen'},
                    {text: '5月', value: '515685', paramname: 'shangpaiyuefen'},
                    {text: '6月', value: '515686', paramname: 'shangpaiyuefen'},
                    {text: '7月', value: '515687', paramname: 'shangpaiyuefen'},
                    {text: '8月', value: '515688', paramname: 'shangpaiyuefen'},
                    {text: '9月', value: '515689', paramname: 'shangpaiyuefen'},
                    {text: '10月', value: '515690', paramname: 'shangpaiyuefen'},
                ]},
                { text: '2015年', value: '2015', paramname: 'buytime', option: [
                    {text: '1月', value: '515681', paramname: 'shangpaiyuefen'},
                    {text: '2月', value: '515682', paramname: 'shangpaiyuefen'},
                    {text: '3月', value: '515683', paramname: 'shangpaiyuefen'},
                    {text: '4月', value: '515684', paramname: 'shangpaiyuefen'},
                    {text: '5月', value: '515685', paramname: 'shangpaiyuefen'},
                    {text: '6月', value: '515686', paramname: 'shangpaiyuefen'},
                    {text: '7月', value: '515687', paramname: 'shangpaiyuefen'},
                    {text: '8月', value: '515688', paramname: 'shangpaiyuefen'},
                    {text: '9月', value: '515689', paramname: 'shangpaiyuefen'},
                    {text: '10月', value: '515690', paramname: 'shangpaiyuefen'},
                ]},
                { text: '2016年', value: '2016', paramname: 'buytime', option: [
                    {text: '1月', value: '515681', paramname: 'shangpaiyuefen'},
                    {text: '2月', value: '515682', paramname: 'shangpaiyuefen'},
                    {text: '3月', value: '515683', paramname: 'shangpaiyuefen'},
                    {text: '4月', value: '515684', paramname: 'shangpaiyuefen'},
                    {text: '5月', value: '515685', paramname: 'shangpaiyuefen'},
                    {text: '6月', value: '515686', paramname: 'shangpaiyuefen'},
                    {text: '7月', value: '515687', paramname: 'shangpaiyuefen'},
                    {text: '8月', value: '515688', paramname: 'shangpaiyuefen'},
                    {text: '9月', value: '515689', paramname: 'shangpaiyuefen'},
                    {text: '10月', value: '515690', paramname: 'shangpaiyuefen'},
                ]},
                { text: '2017年', value: '2017', paramname: 'buytime', option: [
                    {text: '1月', value: '515681', paramname: 'shangpaiyuefen'},
                    {text: '2月', value: '515682', paramname: 'shangpaiyuefen'},
                    {text: '3月', value: '515683', paramname: 'shangpaiyuefen'},
                    {text: '4月', value: '515684', paramname: 'shangpaiyuefen'},
                    {text: '5月', value: '515685', paramname: 'shangpaiyuefen'},
                    {text: '6月', value: '515686', paramname: 'shangpaiyuefen'},
                    {text: '7月', value: '515687', paramname: 'shangpaiyuefen'},
                    {text: '8月', value: '515688', paramname: 'shangpaiyuefen'},
                    {text: '9月', value: '515689', paramname: 'shangpaiyuefen'},
                    {text: '10月', value: '515690', paramname: 'shangpaiyuefen'},
                ]},
                { text: '2018年', value: '2018', paramname: 'buytime', option: [
                    {text: '1月', value: '515681', paramname: 'shangpaiyuefen'},
                    {text: '2月', value: '515682', paramname: 'shangpaiyuefen'},
                    {text: '3月', value: '515683', paramname: 'shangpaiyuefen'},
                    {text: '4月', value: '515684', paramname: 'shangpaiyuefen'},
                    {text: '5月', value: '515685', paramname: 'shangpaiyuefen'},
                    {text: '6月', value: '515686', paramname: 'shangpaiyuefen'},
                    {text: '7月', value: '515687', paramname: 'shangpaiyuefen'},
                    {text: '8月', value: '515688', paramname: 'shangpaiyuefen'},
                    {text: '9月', value: '515689', paramname: 'shangpaiyuefen'},
                    {text: '10月', value: '515690', paramname: 'shangpaiyuefen'},
                ]}
            ]
        };
        instance = TestUtils.renderIntoDocument(
            <PickerSelectorComponent data={timeData} text={{}}></PickerSelectorComponent>
        );
    });
    it('should have its component type with two picker', () => {
        let wheelItems = TestUtils.scryRenderedDOMComponentsWithClass(instance, 'wheelItem');
        expect(wheelItems.length).to.equal(2);
    });
});

describe('PickerSelectorComponentTest of type regionPicker', () => {
    let instance;

    before(() => {
        let regionData = {
            paramname: 'province',
            title: '城市选择',
            option: [
                { text: '北京市', value: '01', paramname: 'province', option: [
                    {text: '市辖区', value: '0101', paramname: 'town', option: [
                        {text: '东城区', value: '010101', paramname: 'area'},
                        {text: '西城区', value: '010102', paramname: 'area'},
                        {text: '朝阳区', value: '010103', paramname: 'area'},
                        {text: '丰台区', value: '010104', paramname: 'area'},
                        {text: '石景山区', value: '010105', paramname: 'area'},
                        {text: '海淀区', value: '010106', paramname: 'area'}
                    ]},
                    {text: '县', value: '0102', paramname: 'town', option: [
                        {text: '密云县', value: '010201', paramname: 'area'},
                        {text: '延庆县', value: '010202', paramname: 'area'}
                    ]},
                ]},
                { text: '河北省', value: '02', paramname: 'province', option: [
                    {text: '石家庄市', value: '0201', paramname: 'town', option: [
                        {text: '长安区', value: '020101', paramname: 'area'},
                        {text: '桥东区', value: '020102', paramname: 'area'},
                        {text: '桥西区', value: '020103', paramname: 'area'},
                        {text: '新华区', value: '020104', paramname: 'area'},
                        {text: '正定区', value: '020105', paramname: 'area'},
                        {text: '栾城区', value: '020106', paramname: 'area'}
                    ]},
                    {text: '唐山市', value: '0202', paramname: 'town', option: [
                        {text: '路南区', value: '020201', paramname: 'area'},
                        {text: '路北区', value: '020202', paramname: 'area'},
                        {text: '古治区', value: '020203', paramname: 'area'},
                        {text: '开平区', value: '020204', paramname: 'area'}
                    ]},
                    {text: '秦皇岛市', value: '0203', paramname: 'town', option: [
                        {text: '海港区', value: '020301', paramname: 'area'},
                        {text: '山海关区', value: '020302', paramname: 'area'},
                        {text: '北戴河区', value: '020303', paramname: 'area'}
                    ]},
                    {text: '邯郸市', value: '0204', paramname: 'town', option: [
                        {text: '邯山区', value: '020401', paramname: 'area'},
                        {text: '丛台区', value: '020402', paramname: 'area'},
                        {text: '复兴区', value: '020403', paramname: 'area'},
                        {text: '邯郸县', value: '020404', paramname: 'area'},
                        {text: '成安县', value: '020405', paramname: 'area'}
                    ]},
                ]}
            ]
        };
        let text = {province: "河北省", town: "唐山市", area: "古治区"};
        instance = TestUtils.renderIntoDocument(
            <PickerSelectorComponent data={regionData} text={text}></PickerSelectorComponent>
        );
    });
    it('should have its component type with three picker', () => {
        let wheelItems = TestUtils.scryRenderedDOMComponentsWithClass(instance, 'wheelItem');
        expect(wheelItems.length).to.equal(3);
    });
    it('its componet should hava three selected value as this.props.text is not null', () => {
        let selectedItem = TestUtils.scryRenderedDOMComponentsWithClass(instance, 'selectItem');
        expect(selectedItem.length).to.equal(3);
    });
    it('its componet should hava selected province as this.props.text.province = 河北省', () => {
        let selectedItem = TestUtils.scryRenderedDOMComponentsWithClass(instance, 'selectItem');
        expect(selectedItem[0].innerHTML).to.equal('河北省');
    });
    it('its componet should hava selected town as this.props.text.town = 唐山市', () => {
        let selectedItem = TestUtils.scryRenderedDOMComponentsWithClass(instance, 'selectItem');
        expect(selectedItem[1].innerHTML).to.equal('唐山市');
    });
    it('its componet should hava selected area as this.props.text.area = 古治区', () => {
        let selectedItem = TestUtils.scryRenderedDOMComponentsWithClass(instance, 'selectItem');
        expect(selectedItem[2].innerHTML).to.equal('古治区');
    });
});