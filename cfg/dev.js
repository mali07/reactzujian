'use strict';

let path = require('path');
let webpack = require('webpack');
let baseConfig = require('./base');
let defaultSettings = require('./defaults');
// Add needed plugins here
let BowerWebpackPlugin = require('bower-webpack-plugin');
let ExtractTextPlugin = require('extract-text-webpack-plugin'); //css单独打包
let marked = require('marked');
let hl = require('highlight.js');
let HTMLWebpackPlugin = require('html-webpack-plugin');

const isProduction = process.env.REACT_WEBPACK_ENV === 'dev';
const codeRenderer = function(code, lang) {
  lang = lang === 'js' ? 'javascript' : lang;
  if (lang === 'html') {
    lang = 'xml';
  }

  let hlCode = lang
    ? hl.highlight(lang, code).value
    : hl.highlightAuto(code).value;

  return `<div class="doc-highlight"><pre>
<code class="${lang || ''}">${hlCode}</code></pre></div>`;
};

let renderer = new marked.Renderer();
renderer.code = codeRenderer;

let config = Object.assign({}, baseConfig, {
  entry: [
    'webpack-dev-server/client?http://127.0.0.1:' + defaultSettings.port,
    'webpack/hot/only-dev-server',
    './examples/index'
  ],
  output: {
    libraryTarget: 'umd',
    path: path.join(__dirname, '/../dist/www'),
    filename: 'app.js',
    publicPath: '/'
  },
  //debug: true,
  cache: false,
  devtool: '#eval-source-map', //false 构建时
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new BowerWebpackPlugin({
      searchResolveModulesDirectories: false
    }),
    new ExtractTextPlugin('app.css'),
    new HTMLWebpackPlugin({
      title: 'CHE UI React',
      template: 'examples/index.ejs',
      UICDN: '', //构建时 这个地方填写部署cdn的路径
      inject: false,
      showErrors: true,
      minify: isProduction
        ? {
            removeComments: true,
            collapseWhitespace: true
          }
        : null
    })
  ],
  module: defaultSettings.getDefaultModules(),
  // watch: !isProduction,
  markdownLoader: {
    renderer: renderer
  },
  transforms: [
    function(file) {
      return through(
        function(buf) {
          this.queue(
            buf
              .split('')
              .map(function(s) {
                return String.fromCharCode(127 - s.charCodeAt(0));
              })
              .join('')
          );
        },
        function() {
          this.queue(null);
        }
      );
    }
  ],
  devServer: {
    contentBase: './examples',
    historyApiFallback: true,
    hot: true,
    open:true,
    port: defaultSettings.port,
    publicPath: '/',
    noInfo: false,
    inline:true,
    hotOnly: true
  },
  externals: {
    'react-router': {
      amd: 'react-router',
      root: 'ReactRouter',
      commonjs: 'react-router',
      commonjs2: 'react-router'
    },
    react: {
      amd: 'react',
      root: 'React',
      commonjs: 'react',
      commonjs2: 'react'
    },
    'react-dom': {
      amd: 'react-dom',
      root: 'ReactDOM',
      commonjs: 'react-dom',
      commonjs2: 'react-dom'
    }
  },
  node: {
    fs: 'empty'
  }
});
config.module.preLoaders.push({
  test: /\.(js|jsx)$/,
  include: path.join(__dirname, '/../examples'),
  loader: 'eslint-loader'
});
config.module.loaders.push({
  test: /\.js$/,
  exclude: /node_modules/,
  loaders: ['react-hot', 'transform/cacheable?brfs', 'babel']
});
config.module.loaders.push({
  test: /\.md$/,
  loader: 'html!markdown'
});

// Add needed loaders to the defaults here
// config.module.loaders.push({
//   test: /\.(js|jsx)$/,
//   loader: 'react-hot-loader!babel-loader!transform-loader/cacheable?brfs',
//   include: [].concat(
//     config.additionalPaths,
//     [ path.join(__dirname, '/../examples') ,path.join(__dirname, '/../src')]
//   )
// })
module.exports = config;
