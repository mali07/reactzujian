'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

require('./close.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var classNames = require('classnames');
var omit = require('object.omit');
var ClassNameMixin = require('./../../mixins/ClassNameMixin');
var Icon = require('./../../components/icon/icon');

var Close = _react2.default.createClass({
    displayName: 'Close',

    mixins: [ClassNameMixin],
    propTypes: {
        classPrefix: _react2.default.PropTypes.string.isRequired,
        component: _react2.default.PropTypes.node,
        spin: _react2.default.PropTypes.bool, //是否使用hover旋转.
        alt: _react2.default.PropTypes.bool, //是否使用边框样式
        icon: _react2.default.PropTypes.bool, //是否使用Icon
        type: _react2.default.PropTypes.string, //按钮类型 默认button
        onClick: _react2.default.PropTypes.function //回调函数
    },
    getDefaultProps: function getDefaultProps() {
        return {
            classPrefix: 'close',
            type: 'button'
        };
    },

    render: function render() {
        var Component = this.props.component || 'button';
        var classSet = this.getClassSet();
        var props = this.props;
        var restProps = omit(this.props, Object.keys(this.constructor.propTypes));

        // transfer type
        if (Component !== 'button') {
            props.type = undefined;
        }

        // className am-close-alt am-close-spin
        classSet[this.prefixClass('alt')] = this.props.alt;
        classSet[this.prefixClass('spin')] = this.props.spin;

        return _react2.default.createElement(
            Component,
            _extends({}, restProps, {
                onClick: props.onClick,
                className: classNames(classSet, this.props.className),
                role: 'close' }),
            this.props.icon ? _react2.default.createElement(Icon, { icon: 'times' }) : '\xD7'
        );
    }
});

module.exports = Close;
//# sourceMappingURL=close.js.map