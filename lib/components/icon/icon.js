'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

require('./icon.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var classNames = require('classnames');
var omit = require('object.omit');
var ClassNameMixin = require('./../../mixins/ClassNameMixin');

var PropTypes = _react2.default.PropTypes;

var Icon = _react2.default.createClass({
    displayName: 'Icon',

    mixins: [ClassNameMixin],

    propTypes: {
        classPrefix: PropTypes.string,
        component: PropTypes.node.isRequired,
        amStyle: PropTypes.string, //提供5种颜色 primary secondary success warning danger，设置 button 属性后有效；
        amSize: PropTypes.string, //提供 3 种大小 sm md lg
        fw: PropTypes.bool, //FontAwesome 在绘制图标的时候不同图标宽度有差异， 添加 fw 将图标设置为固定的宽度，解决宽度不一致问题
        spin: PropTypes.bool, //旋转动画(Chrome 和 Firefox 下， display: inline-block; 或 display: block; 的元素才会应用旋转动画)；
        button: PropTypes.bool, //是否显示为图标按钮
        size: PropTypes.string,
        href: PropTypes.string, //- 链接地址。
        icon: PropTypes.string.isRequired, //图标名称；
        onClick: _react2.default.PropTypes.function //回调函数
    },

    getDefaultProps: function getDefaultProps() {
        return {
            classPrefix: 'icon',
            component: 'span'
        };
    },

    render: function render() {
        var classes = this.getClassSet(true);
        var props = this.props;
        var Component = props.href ? 'a' : props.component;
        var prefixClass = this.prefixClass;
        var setClassNamespace = this.setClassNamespace;
        var restProps = omit(this.props, Object.keys(this.constructor.propTypes));

        // am-icon-[iconName]
        classes[prefixClass(props.icon)] = true;

        // am-icon-btn
        classes[prefixClass('btn')] = props.button;

        // button style
        props.button && props.amStyle && (classes[setClassNamespace(props.amStyle)] = true);

        // am-icon-fw
        classes[prefixClass('fw')] = props.fw;

        // am-icon-spin
        classes[prefixClass('spin')] = props.spin;

        return _react2.default.createElement(
            Component,
            _extends({}, restProps, {
                href: this.props.href,
                onClick: props.onClick,
                className: classNames(classes, this.props.className) }),
            this.props.children
        );
    }
});

module.exports = Icon;
//# sourceMappingURL=icon.js.map