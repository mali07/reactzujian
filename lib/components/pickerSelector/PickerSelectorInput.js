'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _PickerSelector = require('./PickerSelector');

var _PickerSelector2 = _interopRequireDefault(_PickerSelector);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PickerSelectorInput = function (_Component) {
    _inherits(PickerSelectorInput, _Component);

    function PickerSelectorInput(props) {
        _classCallCheck(this, PickerSelectorInput);

        var _this = _possibleConstructorReturn(this, (PickerSelectorInput.__proto__ || Object.getPrototypeOf(PickerSelectorInput)).call(this, props));

        _this.state = { showPicker: false, value: {}, text: {} };
        _this.handleClick = _this.handleClick.bind(_this);
        _this.handleClose = _this.handleClose.bind(_this);
        _this.handleSelect = _this.handleSelect.bind(_this);
        _this.setValue = _this.setValue.bind(_this);
        return _this;
    }

    _createClass(PickerSelectorInput, [{
        key: 'setValue',
        value: function setValue(value, text, children) {
            var self = this;
            var data = self.props.data;

            var default_data = {};
            var default_dataText = {};
            if (value != '') {
                default_data[data.paramname] = value;
                default_dataText[data.paramname] = text;
                if (children) {
                    for (var i = 0; i < children.length; i++) {
                        default_data[children[i].paraname] = children[i].value;
                        default_dataText[children[i].paraname] = children[i].text;
                    }
                }
                self.setState({
                    value: default_data,
                    text: default_dataText
                });
            }
        }
    }, {
        key: 'handleSelect',
        value: function handleSelect() {
            var self = this;
            var _self$state = self.state,
                text = _self$state.text,
                value = _self$state.value;

            var nodes = document.getElementsByClassName('selectItem');
            for (var i = 0; i < nodes.length; i++) {
                value[nodes[i].getAttribute('data-name')] = nodes[i].getAttribute('data-value');
                text[nodes[i].getAttribute('data-name')] = nodes[i].innerText;
            }
            self.setState({
                value: value,
                text: text
            }, function () {
                self.handleClose();
            });
        }
    }, {
        key: 'handleClick',
        value: function handleClick() {
            this.setState({
                showPicker: true
            });
        }
    }, {
        key: 'renderPicker',
        value: function renderPicker() {
            if (this.state.showPicker) {
                return _react2.default.createElement(_PickerSelector2.default, {
                    data: this.props.data,
                    title: this.props.title,
                    text: JSON.parse(JSON.stringify(this.state.text)) // 用于回填展示 避免对象引用赋值
                    , onClose: this.handleClose,
                    onSelect: this.handleSelect
                });
            }
        }
    }, {
        key: 'handleClose',
        value: function handleClose() {
            this.setState({
                showPicker: false
            });
        }
    }, {
        key: 'componentWillMount',
        value: function componentWillMount() {
            var self = this;
            var value = self.props.value;
            if (value && JSON.stringify(value) !== '{}') {
                self.setValue(value.value, value.text, value.children);
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _state = this.state,
                text = _state.text,
                value = _state.value;

            var getText = function getText() {
                var data = [];
                for (var k in text) {
                    data.push(text[k]);
                }
                return data.join('');
            };
            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement('input', {
                    onClick: this.handleClick,
                    placeholder: this.props.placeholder,
                    value: JSON.stringify(value) != '{}' ? getText() : null
                }),
                this.renderPicker()
            );
        }
    }]);

    return PickerSelectorInput;
}(_react.Component);

PickerSelectorInput.propType = {
    data: _propTypes2.default.Object,
    title: _propTypes2.default.String,
    placeholder: _propTypes2.default.String,
    value: _propTypes2.default.Object // Picker回填值
};
PickerSelectorInput.defaultProps = {
    data: {},
    title: '',
    placeholder: '请选择',
    value: {}
};
module.exports = PickerSelectorInput;
//# sourceMappingURL=PickerSelectorInput.js.map