'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _betterScroll = require('better-scroll');

var _betterScroll2 = _interopRequireDefault(_betterScroll);

require('./../../styles/pickerSelector/pickerSelector.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PickerSelector = function (_Component) {
    _inherits(PickerSelector, _Component);

    function PickerSelector(props) {
        _classCallCheck(this, PickerSelector);

        var _this = _possibleConstructorReturn(this, (PickerSelector.__proto__ || Object.getPrototypeOf(PickerSelector)).call(this, props));

        _this.state = {
            cacheChoose: {
                text: {},
                value: {}
            },
            pickerData: {}
        };
        _this.cacheMsg = { scroll: [], distance: '', data: _this.props.data };
        return _this;
    }

    _createClass(PickerSelector, [{
        key: 'findSelectItem',
        value: function findSelectItem(self, data, txt) {
            var cacheChoose = self.state.cacheChoose.text;
            var result = false;
            if (cacheChoose[data[0].paramname] == txt) result = true;
            return result;
        }
    }, {
        key: 'renderWheel',
        value: function renderWheel(data) {
            var self = this;
            return _react2.default.createElement(
                'div',
                { className: 'wheelItem' },
                _react2.default.createElement(
                    'ul',
                    { className: 'wheel-scroll', 'data-index': 0 },
                    data.map(function (item, index) {
                        return _react2.default.createElement(
                            'li',
                            {
                                key: index,
                                'data-name': item.paramname,
                                'data-value': item.value,
                                'data-index': index,
                                className: (0, _classnames2.default)({
                                    'wheel-item': true,
                                    selectItem: self.findSelectItem(self, data, item.text)
                                }) },
                            item.text
                        );
                    })
                )
            );
        }
    }, {
        key: 'componentWillMount',
        value: function componentWillMount() {
            var self = this;
            var _self$state = self.state,
                pickerData = _self$state.pickerData,
                cacheChoose = _self$state.cacheChoose;

            var cacheData = self.cacheMsg.data;
            var text = self.props.text;
            // 如果选择器大于1级，则初始化各滚轮数据
            if (JSON.stringify(text) == '{}' && cacheData.option[0].option) {
                var data = cacheData.option[0].option;
                pickerData[data[0].paramname] = data;
                if (data[0].option) {
                    pickerData[data[0].option[0].paramname] = data[0].option;
                }
            } else {
                cacheChoose.text = text;
                for (var item in cacheData.option) {
                    if (cacheData.option[item].option && cacheData.option[item].text == text[cacheData.paramname]) {
                        var data02 = cacheData.option[item].option; // 2级数据
                        pickerData[data02[0].paramname] = data02;
                        for (var index in data02) {
                            if (data02[index].option && data02[index].text == text[data02[0].paramname]) {
                                var data03 = data02[index].option;
                                pickerData[data03[0].paramname] = data03;
                            }
                        }
                    }
                }
            }
            self.setState({
                pickerData: pickerData,
                cacheChoose: cacheChoose
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var self = this;
            var data = self.cacheMsg.data;
            return _react2.default.createElement(
                'div',
                { className: 'pickerWrap' },
                _react2.default.createElement(
                    'div',
                    { className: 'pickerMark' },
                    ' '
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'pickerMain' },
                    _react2.default.createElement(
                        'div',
                        { className: 'pickerHeader' },
                        _react2.default.createElement(
                            'span',
                            { className: 's0', onClick: self.props.onClose },
                            '\u53D6\u6D88'
                        ),
                        _react2.default.createElement(
                            'span',
                            { className: 's1' },
                            self.props.title
                        ),
                        _react2.default.createElement(
                            'span',
                            { className: 's2 done', onClick: self.props.onSelect },
                            '\u786E\u5B9A'
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'pickerContent' },
                        _react2.default.createElement(
                            'div',
                            { className: 'markTop' },
                            ' '
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'markBottom' },
                            ' '
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'pickerWheel', ref: 'wheel' },
                            self.renderWheel(data.option),
                            JSON.stringify(self.state.pickerData) !== '{}' && Object.keys(self.state.pickerData).map(function (key) {
                                return self.renderWheel(self.state.pickerData[key]);
                            })
                        )
                    )
                )
            );
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.scrollControl();
        }
    }, {
        key: 'scrollControl',
        value: function scrollControl() {
            var self = this;
            var _self$state2 = self.state,
                cacheChoose = _self$state2.cacheChoose,
                pickerData = _self$state2.pickerData;
            var _self$cacheMsg = self.cacheMsg,
                scroll = _self$cacheMsg.scroll,
                distance = _self$cacheMsg.distance;

            var childs = self.refs.wheel.children;
            for (var i = 0; i < childs.length; i++) {
                scroll[i] = new _betterScroll2.default(childs[i], {
                    wheel: {
                        selectedIndex: 0,
                        rotate: 25,
                        adjustTime: 400,
                        /** 默认值就是下面配置的两个，为了展示二者的作用，这里再配置一下 */
                        wheelWrapperClass: 'wheel-scroll',
                        wheelItemClass: 'wheel-item'
                    },
                    probeType: 3
                });
            }
            // 获取li高度并存入缓存
            var liNode = childs[0].getElementsByTagName('li')[0];
            distance = liNode.getBoundingClientRect().height;
            self.initPickeState(self, cacheChoose, scroll, distance);

            var _loop = function _loop(_i) {
                scroll[_i].on('scrollEnd', function () {
                    // 计算滚动结束的li的索引 并根据索引获取当前选中DOM元素
                    var index = -Math.round(scroll[_i].y / distance);
                    var node = scroll[_i].scroller.children[index];
                    if (!node) {
                        // 如果当前滚动位置超过li索引 则默认选中当前Picker最后一个DOM元素
                        var len = scroll[_i].scroller.children.length - 1;
                        node = scroll[_i].scroller.children[len];
                    }
                    scroll[_i].scrollTo(0, -index * distance);
                    cacheChoose.value[node.getAttribute('data-name')] = node.getAttribute('data-value');
                    cacheChoose.text[node.getAttribute('data-name')] = node.innerText;
                    if (_i !== scroll.length - 1) {
                        // 除了Picker第一级数据 其余级数据需要级联变化
                        self.cascadScroll(_i, self, index, pickerData, node, cacheChoose, scroll);
                    } else {
                        self.setState({ cacheChoose: cacheChoose });
                    }
                });
            };

            for (var _i = 0; _i < scroll.length; _i++) {
                _loop(_i);
            }
        }
    }, {
        key: 'cascadScroll',
        value: function cascadScroll(i, self, index, pickerData, node, cacheChoose, scroll) {
            var data = void 0;
            if (i === 0) {
                data = self.cacheMsg.data.option[index].option;
            } else {
                data = pickerData[node.getAttribute('data-name')][index].option;
            }
            cacheChoose.value[data[0].paramname] = data[0].value;
            cacheChoose.text[data[0].paramname] = data[0].text;
            pickerData[data[0].paramname] = data;
            if (data[0].option) {
                var data03 = data[0].option;
                cacheChoose.value[data03[0].paramname] = data03[0].value;
                cacheChoose.text[data03[0].paramname] = data03[0].text;
                pickerData[data03[0].paramname] = data03;
            }
            self.setState({
                cacheChoose: cacheChoose,
                pickerData: pickerData
            });
            scroll[i + 1].scrollTo(0, 0);
            scroll[i + 1].refresh();
            if (scroll.length > 2) {
                scroll[scroll.length - 1].scrollTo(0, 0);
            }
        }
        // 初始化Picker滚动状态

    }, {
        key: 'initPickeState',
        value: function initPickeState(self, cacheChoose, scroll, distance) {
            // 如果当前props.value为空，即未选择任何时间 初始化焦点在各滚轮的第一个数据上
            if (JSON.stringify(self.props.text) === '{}') {
                var cacheData = self.cacheMsg.data;
                var initData = function initData(datas) {
                    cacheChoose.text[datas[0].paramname] = datas[0].text;
                    cacheChoose.value[datas[0].paramname] = datas[0].value;
                };
                cacheChoose.text[cacheData.paramname] = cacheData.option[0].text;
                cacheChoose.value[cacheData.paramname] = cacheData.option[0].value;
                if (cacheData.option[0].option) {
                    var data = cacheData.option[0].option;
                    initData(data);
                    if (data[0].option) {
                        initData(data[0].option);
                    }
                }
                self.setState({ cacheChoose: cacheChoose });
            } else {
                // 如果有用户选择的时间 则自动将焦点滚动到选择的位置
                var selectNodes = document.getElementsByClassName('selectItem');
                for (var i = 0; i < selectNodes.length; i++) {
                    var index = selectNodes[i].getAttribute('data-index');
                    scroll[i].scrollTo(0, -index * distance);
                }
            }
        }
    }]);

    return PickerSelector;
}(_react.Component);

PickerSelector.propTypes = {
    data: _propTypes2.default.object,
    text: _propTypes2.default.object
};
PickerSelector.defaultProps = {
    data: {},
    text: {}
};
module.exports = PickerSelector;
//# sourceMappingURL=PickerSelector.js.map