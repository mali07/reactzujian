'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

require('./../../styles/toast/Toast.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ToastComponent = function (_Component) {
    _inherits(ToastComponent, _Component);

    function ToastComponent(props) {
        _classCallCheck(this, ToastComponent);

        var _this = _possibleConstructorReturn(this, (ToastComponent.__proto__ || Object.getPrototypeOf(ToastComponent)).call(this, props));

        _this.state = {
            show: true,
            type: props.type || 0, //0 1 2 Toast三种状态 0：纯文字 1：带图标 ok 2：带图标 error
            toastText: props.text || '' //提示文本
        };
        _this.showToast = _this.showToast.bind(_this);
        return _this;
    }

    _createClass(ToastComponent, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var self = this;
            setTimeout(function () {
                self.setState({
                    show: false
                });
            }, 1500); //默认1.5s后关闭
        }
    }, {
        key: 'showToast',
        value: function showToast(text, type) {
            var self = this;
            self.setState({
                show: true,
                toastText: text,
                type: type
            }, function () {
                setTimeout(function () {
                    self.setState({
                        show: false
                    });
                }, 1500);
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var show = this.state.show;
            var _img = null;

            if (this.state.type == 1) {
                _img = _react2.default.createElement('img', { src: '//img.58cdn.com.cn/images/guchejia_app/toast-ok.png' });
            } else if (this.state.type == 2) {
                var img = require('./../../images/icon-close.png');
                _img = _react2.default.createElement('img', { src: img });
            }

            if (show) {
                return _react2.default.createElement(
                    'div',
                    { className: 'toast-container' },
                    _react2.default.createElement(
                        'div',
                        { className: (0, _classnames2.default)({ toast: true, 'only-text': !this.state.type }) },
                        _img,
                        _react2.default.createElement(
                            'p',
                            null,
                            this.state.toastText
                        )
                    )
                );
            } else {
                return null;
            }
        }
    }]);

    return ToastComponent;
}(_react.Component);

var ToastInstance = function ToastInstance(text, type) {
    return _react2.default.createElement(ToastComponent, { ref: 'toastRef', text: text, type: type });
};
ToastComponent.displayName = 'ToastToastComponent';
function showToast(text, type) {
    var toastNode = document.createElement('div');
    document.body.appendChild(toastNode);
    _reactDom2.default.render(ToastInstance(text, type), toastNode);
}
ToastComponent.propTypes = {
    text: _propTypes2.default.string,
    type: _propTypes2.default.number,
    icon: _propTypes2.default.string
};
ToastComponent.defaultProps = {
    text: '',
    type: 0, //0 1 2 Toast三种状态 0：纯文字 1：带图标 ok 2：带图标 error
    icon: ''
};
module.exports = { showToast: showToast };
//# sourceMappingURL=ToastComponent.js.map