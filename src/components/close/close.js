'use strict';
import React from 'react';
var classNames = require('classnames');
var omit = require('object.omit');
var ClassNameMixin = require('./../../mixins/ClassNameMixin');
var Icon = require('./../../components/icon/icon');
import './close.scss';
var Close = React.createClass({
    mixins: [ClassNameMixin],
    propTypes: {
        classPrefix: React.PropTypes.string.isRequired,
        component: React.PropTypes.node,
        spin: React.PropTypes.bool, //是否使用hover旋转.
        alt: React.PropTypes.bool, //是否使用边框样式
        icon: React.PropTypes.bool, //是否使用Icon
        type: React.PropTypes.string, //按钮类型 默认button
        onClick: React.PropTypes.function //回调函数
    },
    getDefaultProps: function() {
        return {
            classPrefix: 'close',
            type: 'button'
        };
    },

    render: function() {
        var Component = this.props.component || 'button';
        var classSet = this.getClassSet();
        var props = this.props;
        var restProps = omit(this.props, Object.keys(this.constructor.propTypes));

        // transfer type
        if (Component !== 'button') {
            props.type = undefined;
        }

        // className am-close-alt am-close-spin
        classSet[this.prefixClass('alt')] = this.props.alt;
        classSet[this.prefixClass('spin')] = this.props.spin;

        return (
            <Component
                {...restProps}
                onClick={props.onClick}
                className={classNames(classSet, this.props.className)}
                role="close">
                {this.props.icon ? <Icon icon="times" /> : '\u00D7'}
            </Component>
        );
    }
});

module.exports = Close;
