'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PickerSelector from './PickerSelector';

class PickerSelectorInput extends Component {
    constructor(props) {
        super(props);
        this.state = { showPicker: false, value: {}, text: {} };
        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.setValue = this.setValue.bind(this);
    }
    setValue(value, text, children) {
        const self = this;
        let { data } = self.props;
        let default_data = {};
        let default_dataText = {};
        if (value != '') {
            default_data[data.paramname] = value;
            default_dataText[data.paramname] = text;
            if (children) {
                for (let i = 0; i < children.length; i++) {
                    default_data[children[i].paraname] = children[i].value;
                    default_dataText[children[i].paraname] = children[i].text;
                }
            }
            self.setState({
                value: default_data,
                text: default_dataText
            });
        }
    }
    handleSelect() {
        const self = this;
        let { text, value } = self.state;
        let nodes = document.getElementsByClassName('selectItem');
        for (let i = 0; i < nodes.length; i++) {
            value[nodes[i].getAttribute('data-name')] = nodes[i].getAttribute('data-value');
            text[nodes[i].getAttribute('data-name')] = nodes[i].innerText;
        }
        self.setState(
            {
                value: value,
                text: text
            },
            function() {
                self.handleClose();
            }
        );
    }
    handleClick() {
        this.setState({
            showPicker: true
        });
    }
    renderPicker() {
        if (this.state.showPicker) {
            return (
                <PickerSelector
                    data={this.props.data}
                    title={this.props.title}
                    text={JSON.parse(JSON.stringify(this.state.text))} // 用于回填展示 避免对象引用赋值
                    onClose={this.handleClose}
                    onSelect={this.handleSelect}
                />
            );
        }
    }
    handleClose() {
        this.setState({
            showPicker: false
        });
    }
    componentWillMount() {
        let self = this;
        let value = self.props.value;
        if (value && JSON.stringify(value) !== '{}') {
            self.setValue(value.value, value.text, value.children);
        }
    }
    render() {
        let { text, value } = this.state;
        let getText = () => {
            let data = [];
            for (let k in text) {
                data.push(text[k]);
            }
            return data.join('');
        };
        return (
            <div>
                <input
                    onClick={this.handleClick}
                    placeholder={this.props.placeholder}
                    value={JSON.stringify(value) != '{}' ? getText() : null}
                />
                {this.renderPicker()}
            </div>
        );
    }
}
PickerSelectorInput.propType = {
    data: PropTypes.Object,
    title: PropTypes.String,
    placeholder: PropTypes.String,
    value: PropTypes.Object // Picker回填值
};
PickerSelectorInput.defaultProps = {
    data: {},
    title: '',
    placeholder: '请选择',
    value: {}
};
module.exports = PickerSelectorInput;
