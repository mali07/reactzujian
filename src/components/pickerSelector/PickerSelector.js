'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cs from 'classnames';
import BScroll from 'better-scroll';
import './../../styles/pickerSelector/pickerSelector.scss';

class PickerSelector extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cacheChoose: {
                text: {},
                value: {}
            },
            pickerData: {}
        };
        this.cacheMsg = { scroll: [], distance: '', data: this.props.data };
    }

    findSelectItem(self, data, txt) {
        let cacheChoose = self.state.cacheChoose.text;
        let result = false;
        if (cacheChoose[data[0].paramname] == txt) result = true;
        return result;
    }
    renderWheel(data) {
        let self = this;
        return (
            <div className="wheelItem">
                <ul className="wheel-scroll" data-index={0}>
                    {data.map((item, index) => {
                        return (
                            <li
                                key={index}
                                data-name={item.paramname}
                                data-value={item.value}
                                data-index={index}
                                className={cs({
                                    'wheel-item': true,
                                    selectItem: self.findSelectItem(self, data, item.text)
                                })}>
                                {item.text}
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
    componentWillMount() {
        let self = this;
        let { pickerData, cacheChoose } = self.state;
        let cacheData = self.cacheMsg.data;
        let text = self.props.text;
        // 如果选择器大于1级，则初始化各滚轮数据
        if (JSON.stringify(text) == '{}' && cacheData.option[0].option) {
            let data = cacheData.option[0].option;
            pickerData[data[0].paramname] = data;
            if (data[0].option) {
                pickerData[data[0].option[0].paramname] = data[0].option;
            }
        } else {
            cacheChoose.text = text;
            for (let item in cacheData.option) {
                if (
                    cacheData.option[item].option &&
                    cacheData.option[item].text == text[cacheData.paramname]
                ) {
                    let data02 = cacheData.option[item].option; // 2级数据
                    pickerData[data02[0].paramname] = data02;
                    for (let index in data02) {
                        if (
                            data02[index].option &&
                            data02[index].text == text[data02[0].paramname]
                        ) {
                            let data03 = data02[index].option;
                            pickerData[data03[0].paramname] = data03;
                        }
                    }
                }
            }
        }
        self.setState({
            pickerData: pickerData,
            cacheChoose: cacheChoose
        });
    }
    render() {
        let self = this;
        let data = self.cacheMsg.data;
        return (
            <div className="pickerWrap">
                <div className="pickerMark"> </div>
                <div className="pickerMain">
                    <div className="pickerHeader">
                        <span className="s0" onClick={self.props.onClose}>
                            取消
                        </span>
                        <span className="s1">{self.props.title}</span>
                        <span className="s2 done" onClick={self.props.onSelect}>
                            确定
                        </span>
                    </div>
                    <div className="pickerContent">
                        <div className="markTop"> </div>
                        <div className="markBottom"> </div>
                        <div className="pickerWheel" ref="wheel">
                            {self.renderWheel(data.option)}
                            {JSON.stringify(self.state.pickerData) !== '{}' &&
                                Object.keys(self.state.pickerData).map(key => {
                                    return self.renderWheel(self.state.pickerData[key]);
                                })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    componentDidMount() {
        this.scrollControl();
    }
    scrollControl() {
        const self = this;
        let { cacheChoose, pickerData } = self.state;
        let { scroll, distance } = self.cacheMsg;
        let childs = self.refs.wheel.children;
        for (let i = 0; i < childs.length; i++) {
            scroll[i] = new BScroll(childs[i], {
                wheel: {
                    selectedIndex: 0,
                    rotate: 25,
                    adjustTime: 400,
                    /** 默认值就是下面配置的两个，为了展示二者的作用，这里再配置一下 */
                    wheelWrapperClass: 'wheel-scroll',
                    wheelItemClass: 'wheel-item'
                },
                probeType: 3
            });
        }
        // 获取li高度并存入缓存
        let liNode = childs[0].getElementsByTagName('li')[0];
        distance = liNode.getBoundingClientRect().height;
        self.initPickeState(self, cacheChoose, scroll, distance);
        for (let i = 0; i < scroll.length; i++) {
            scroll[i].on('scrollEnd', function() {
                // 计算滚动结束的li的索引 并根据索引获取当前选中DOM元素
                let index = -Math.round(scroll[i].y / distance);
                let node = scroll[i].scroller.children[index];
                if (!node) {
                    // 如果当前滚动位置超过li索引 则默认选中当前Picker最后一个DOM元素
                    let len = scroll[i].scroller.children.length - 1;
                    node = scroll[i].scroller.children[len];
                }
                scroll[i].scrollTo(0, -index * distance);
                cacheChoose.value[node.getAttribute('data-name')] = node.getAttribute('data-value');
                cacheChoose.text[node.getAttribute('data-name')] = node.innerText;
                if (i !== scroll.length - 1) {
                    // 除了Picker第一级数据 其余级数据需要级联变化
                    self.cascadScroll(i, self, index, pickerData, node, cacheChoose, scroll);
                } else {
                    self.setState({ cacheChoose: cacheChoose });
                }
            });
        }
    }
    cascadScroll(i, self, index, pickerData, node, cacheChoose, scroll) {
        let data;
        if (i === 0) {
            data = self.cacheMsg.data.option[index].option;
        } else {
            data = pickerData[node.getAttribute('data-name')][index].option;
        }
        cacheChoose.value[data[0].paramname] = data[0].value;
        cacheChoose.text[data[0].paramname] = data[0].text;
        pickerData[data[0].paramname] = data;
        if (data[0].option) {
            let data03 = data[0].option;
            cacheChoose.value[data03[0].paramname] = data03[0].value;
            cacheChoose.text[data03[0].paramname] = data03[0].text;
            pickerData[data03[0].paramname] = data03;
        }
        self.setState({
            cacheChoose: cacheChoose,
            pickerData: pickerData
        });
        scroll[i + 1].scrollTo(0, 0);
        scroll[i + 1].refresh();
        if (scroll.length > 2) {
            scroll[scroll.length - 1].scrollTo(0, 0);
        }
    }
    // 初始化Picker滚动状态
    initPickeState(self, cacheChoose, scroll, distance) {
        // 如果当前props.value为空，即未选择任何时间 初始化焦点在各滚轮的第一个数据上
        if (JSON.stringify(self.props.text) === '{}') {
            let cacheData = self.cacheMsg.data;
            let initData = function(datas) {
                cacheChoose.text[datas[0].paramname] = datas[0].text;
                cacheChoose.value[datas[0].paramname] = datas[0].value;
            };
            cacheChoose.text[cacheData.paramname] = cacheData.option[0].text;
            cacheChoose.value[cacheData.paramname] = cacheData.option[0].value;
            if (cacheData.option[0].option) {
                let data = cacheData.option[0].option;
                initData(data);
                if (data[0].option) {
                    initData(data[0].option);
                }
            }
            self.setState({ cacheChoose: cacheChoose });
        } else {
            // 如果有用户选择的时间 则自动将焦点滚动到选择的位置
            let selectNodes = document.getElementsByClassName('selectItem');
            for (let i = 0; i < selectNodes.length; i++) {
                let index = selectNodes[i].getAttribute('data-index');
                scroll[i].scrollTo(0, -index * distance);
            }
        }
    }
}

PickerSelector.propTypes = {
    data: PropTypes.object,
    text: PropTypes.object
};
PickerSelector.defaultProps = {
    data: {},
    text: {}
};
module.exports = PickerSelector;
