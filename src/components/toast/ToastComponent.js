'use strict';
import React, { Component } from 'react';
import ReactDom from 'react-dom';
import PropTypes from 'prop-types';
import cs from 'classnames';
import './../../styles/toast/Toast.scss';
class ToastComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: true,
            type: props.type || 0, //0 1 2 Toast三种状态 0：纯文字 1：带图标 ok 2：带图标 error
            toastText: props.text || '' //提示文本
        };
        this.showToast = this.showToast.bind(this);
    }

    componentDidMount() {
        let self = this;
        setTimeout(function() {
            self.setState({
                show: false
            });
        }, 1500); //默认1.5s后关闭
    }

    showToast(text, type) {
        let self = this;
        self.setState(
            {
                show: true,
                toastText: text,
                type: type
            },
            () => {
                setTimeout(function() {
                    self.setState({
                        show: false
                    });
                }, 1500);
            }
        );
    }

    render() {
        let show = this.state.show;
        let _img = null;

        if (this.state.type == 1) {
            _img = <img src="//img.58cdn.com.cn/images/guchejia_app/toast-ok.png" />;
        } else if (this.state.type == 2) {
            var img = require('./../../images/icon-close.png');
            _img = <img src={img} />;
        }

        if (show) {
            return (
                <div className="toast-container">
                    <div className={cs({ toast: true, 'only-text': !this.state.type })}>
                        {_img}
                        <p>{this.state.toastText}</p>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }
}
const ToastInstance = (text, type) => {
    return <ToastComponent ref="toastRef" text={text} type={type} />;
};
ToastComponent.displayName = 'ToastToastComponent';
function showToast(text, type) {
    let toastNode = document.createElement('div');
    document.body.appendChild(toastNode);
    ReactDom.render(ToastInstance(text, type), toastNode);
}
ToastComponent.propTypes = {
    text: PropTypes.string,
    type: PropTypes.number,
    icon: PropTypes.string
};
ToastComponent.defaultProps = {
    text: '',
    type: 0, //0 1 2 Toast三种状态 0：纯文字 1：带图标 ok 2：带图标 error
    icon: ''
};
module.exports = { showToast };
