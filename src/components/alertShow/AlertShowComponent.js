'use strict';

import React from 'react';

require('styles/alertShow/AlertShow.scss');

class AlertShowComponent extends React.Component {
    render() {
        return (
            <div className="alertshow-component">
                Please edit src/components/alertShow//AlertShowComponent.js to update this
                component!
            </div>
        );
    }
}

AlertShowComponent.displayName = 'AlertShowAlertShowComponent';

// Uncomment properties you need
// AlertShowComponent.propTypes = {};
// AlertShowComponent.defaultProps = {};

export default AlertShowComponent;
