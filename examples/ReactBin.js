'use strict';
var React =  require('react');
var ReactDOM = require('react-dom');
var babel = require('babel-standalone');
var CodeMirror = require('codemirror');
require('codemirror/mode/javascript/javascript');
require('codemirror/addon/runmode/runmode');
/**引入组件模块区域 */
var Close = require('components/close');
var Icon = require('components/icon');
var Toast = require('components/toast');
var PickerSelector = require('components/pickerSelector').PickerSelector;
var PickerSelectorInput = require('components/pickerSelector').PickerSelectorInput;
/**引入组件模块区域 */
var IS_MOBILE = typeof navigator !== 'undefined' && (
    navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)
  );

var CodeExample = React.createClass({
  componentDidMount: function() {
    if (CodeMirror === undefined) {
      return;
    }

    CodeMirror.runMode(
      this.props.code,
      this.props.mode,
      ReactDOM.findDOMNode(this).children[0]
    );
  },

  render: function() {
    return (
      <pre className="cm-s-default">
        <code>
          {this.props.code}
        </code>
      </pre>
    );
  }
});

var CodeEditor = React.createClass({
  propTypes: {
    readOnly: React.PropTypes.bool,
    code: React.PropTypes.string
  },

  componentDidMount: function() {
    if (IS_MOBILE || CodeMirror === undefined) {
      return;
    }

    this.editor = CodeMirror.fromTextArea(this.refs.editor, {
      mode: 'javascript',
      lineNumbers: false,
      lineWrapping: false,
      matchBrackets: true,
      tabSize: 2,
      readOnly: this.props.readOnly
    });

    this.editor.on('change', this.handleChange);
  },

  componentDidUpdate: function() {
    if (this.props.readOnly) {
      this.editor.setValue(this.props.code);
    }
  },

  handleChange: function() {
    if (!this.props.readOnly && this.props.onChange) {
      this.props.onChange(this.editor.getValue());
    }
  },

  render: function() {
    var editor;

    if (IS_MOBILE) {
      editor = (
        <CodeExample mode="javascript" code={this.props.code}/>
      );
    } else {
      editor = <textarea ref='editor' defaultValue={this.props.code} />;
    }

    return (
      <div style={this.props.style} className={this.props.className}>
        {editor}
      </div>
    );
  }
});

var SelfCleaningTimeoutMixin = {
  componentDidUpdate: function() {
    clearTimeout(this.timeoutID);
  },

  setTimeout: function() {
    clearTimeout(this.timeoutID);
    this.timeoutID = setTimeout.apply(null, arguments);
  }
};

var ReactBin = React.createClass({
  mixins: [SelfCleaningTimeoutMixin],

  propTypes: {
    code: React.PropTypes.string.isRequired,
    renderCode: React.PropTypes.bool
  },

  getInitialState: function() {
    return {
      code: this.props.code
    };
  },

  componentDidMount: function() {
    this.executeCode();
  },

  componentWillUpdate: function(nextProps, nextState) {
    if (this.state.code !== nextState.code) {
      this.executeCode();
    }
  },

  componentWillUnmount: function() {
    var mountNode = ReactDOM.findDOMNode(this.refs.example);

    try {
      ReactDOM.unmountComponentAtNode(mountNode);
    } catch (e) {
      console.error(e);
    }
  },

  executeCode: function() {
    var mountNode = this.refs.example;

    try {
      ReactDOM.unmountComponentAtNode(mountNode);
    } catch (e) {
      console.error(e);
    }

    try {
      var code = babel.transform(this.state.code, {
        presets: ['react']
      }).code;

      if (this.props.renderCode) {
        ReactDOM.render(<CodeEditor code={code} readOnly={true} />, mountNode);
      } else {
        /* eslint-disable */
        eval(code);
        /* eslint-enable */
      }
    } catch (err) {
      this.setTimeout(function() {
        // console.log(err.toString());
        console.log(err.stack);
      }, 500);
    }
  },

  handleCodeChange: function(val) {
    this.setState({code: val});
    this.executeCode();
  },

  render: function() {
    return (
      <div className='doc-codebin'>
        <div className='doc-example'>
          <div ref='example' />
        </div>
        <CodeEditor
          key='jsx'
          onChange={this.handleCodeChange}
          className='doc-code'
          code={this.state.code}/>
      </div>
    );
  }
});

module.exports = ReactBin;
