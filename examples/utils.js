'use strict';

var React = require('react');
var classnames = require('classnames');
// var PropTypes =require('prop-types');
// React.PropTypes = PropTypes;//16.0类型检测兼容React.PropTypes.bool 写法 15.5之后独立成prop-types
// var createReactClass = require('create-react-class');
// React.createClass = createReactClass;//兼容createClass写法
exports.Markdown = React.createClass({
  propTypes: {
    doc: React.PropTypes.bool
  },

  // TODO:
  //   1. render markdown
  //   2. highlight code
  render: function() {
    var Tag = this.props.doc ? exports.Doc : 'div';

    return (
      <Tag
        dangerouslySetInnerHTML={{__html: this.props.children}}
        className={this.props.className || null}
      />
    );
  }
});

exports.Doc = React.createClass({
  render: function() {
    return (
      <div
        {...this.props}
        className={classnames('doc-content', this.props.className)}
      >
        {this.props.children}
      </div>
    );
  }
});

exports.getAssetsPath = function() {
  return '';
};

exports.isProduction = process.env.NODE_ENV === 'production';
