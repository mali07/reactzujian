'use strict';
import React from 'react';
import {Doc} from '../utils';
var Markdown = require('../utils').Markdown;
var ReactBin = require('../ReactBin');
var examples = {
  basic: require('fs').readFileSync(__dirname + '/01-basic.js', 'utf-8'),
  timePicker: require('fs').readFileSync(__dirname + '/02-timePicker.js', 'utf-8'),
  regionPicker: require('fs').readFileSync(__dirname + '/03-regionPicker.js', 'utf-8')
};

class CloseComp extends React.Component {
    constructor(props){
      super(props)
    }
    render(){
       return (
        <Doc>
          <h1>PickerSelector</h1>
          <hr />
          <h2>组件介绍</h2>
          <Markdown>{require('./01-intro.md')}</Markdown>
          <h2>组件演示</h2>
          <h3>普通选择器</h3>
          <ReactBin code={examples.basic} />
          <h3>时间选择器</h3>
          <ReactBin code={examples.timePicker} />
          <h3>省市区选择器</h3>
          <ReactBin code={examples.regionPicker} />
        </Doc>
       )
    }
}
module.exports = CloseComp;
