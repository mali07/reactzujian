var options = {
    paramname: 'province',
    title: '城市选择',
    option: [
        { text: '北京市', value: '01', paramname: 'province', option: [
            {text: '市辖区', value: '0101', paramname: 'town', option: [
                {text: '东城区', value: '010101', paramname: 'area'},
                {text: '西城区', value: '010102', paramname: 'area'},
                {text: '朝阳区', value: '010103', paramname: 'area'},
                {text: '丰台区', value: '010104', paramname: 'area'},
                {text: '石景山区', value: '010105', paramname: 'area'},
                {text: '海淀区', value: '010106', paramname: 'area'}
            ]},
            {text: '县', value: '0102', paramname: 'town', option: [
                {text: '密云县', value: '010201', paramname: 'area'},
                {text: '延庆县', value: '010202', paramname: 'area'}
            ]},
        ]},
        { text: '河北省', value: '02', paramname: 'province', option: [
            {text: '石家庄市', value: '0201', paramname: 'town', option: [
                {text: '长安区', value: '020101', paramname: 'area'},
                {text: '桥东区', value: '020102', paramname: 'area'},
                {text: '桥西区', value: '020103', paramname: 'area'},
                {text: '新华区', value: '020104', paramname: 'area'},
                {text: '正定区', value: '020105', paramname: 'area'},
                {text: '栾城区', value: '020106', paramname: 'area'}
            ]},
            {text: '唐山市', value: '0202', paramname: 'town', option: [
                {text: '路南区', value: '020201', paramname: 'area'},
                {text: '路北区', value: '020202', paramname: 'area'},
                {text: '古治区', value: '020203', paramname: 'area'},
                {text: '开平区', value: '020204', paramname: 'area'}
            ]},
            {text: '秦皇岛市', value: '0203', paramname: 'town', option: [
                {text: '海港区', value: '020301', paramname: 'area'},
                {text: '山海关区', value: '020302', paramname: 'area'},
                {text: '北戴河区', value: '020303', paramname: 'area'}
            ]},
            {text: '邯郸市', value: '0204', paramname: 'town', option: [
                {text: '邯山区', value: '020401', paramname: 'area'},
                {text: '丛台区', value: '020402', paramname: 'area'},
                {text: '复兴区', value: '020403', paramname: 'area'},
                {text: '邯郸县', value: '020404', paramname: 'area'},
                {text: '成安县', value: '020405', paramname: 'area'}
            ]},
        ]}
    ]
  };
  var props = {
    data: options,
    title: '城市选择',
    value: {"children":[{"paraname":"town","text":"唐山市","value":"0202"},{"paraname":"area","text":"古治区","value":"020203"}],"paraname":"province","text":"河北省","value":"02"}
  }
  var pickerSelectInputInstance = (
    <div>
        <PickerSelectorInput {...props}/>
    </div>
  );
  ReactDOM.render(pickerSelectInputInstance, mountNode);
    