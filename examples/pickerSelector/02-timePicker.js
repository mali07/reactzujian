var options = {
    paramname: 'buytime',
    title: '购车时间',
    option: [
        { text: '2014年', value: '2014', paramname: 'buytime', option: [
            {text: '1月', value: '515681', paramname: 'shangpaiyuefen'},
            {text: '2月', value: '515682', paramname: 'shangpaiyuefen'},
            {text: '3月', value: '515683', paramname: 'shangpaiyuefen'},
            {text: '4月', value: '515684', paramname: 'shangpaiyuefen'},
            {text: '5月', value: '515685', paramname: 'shangpaiyuefen'},
            {text: '6月', value: '515686', paramname: 'shangpaiyuefen'},
            {text: '7月', value: '515687', paramname: 'shangpaiyuefen'},
            {text: '8月', value: '515688', paramname: 'shangpaiyuefen'},
            {text: '9月', value: '515689', paramname: 'shangpaiyuefen'},
            {text: '10月', value: '515690', paramname: 'shangpaiyuefen'},
        ]},
        { text: '2015年', value: '2015', paramname: 'buytime', option: [
            {text: '1月', value: '515681', paramname: 'shangpaiyuefen'},
            {text: '2月', value: '515682', paramname: 'shangpaiyuefen'},
            {text: '3月', value: '515683', paramname: 'shangpaiyuefen'},
            {text: '4月', value: '515684', paramname: 'shangpaiyuefen'},
            {text: '5月', value: '515685', paramname: 'shangpaiyuefen'},
            {text: '6月', value: '515686', paramname: 'shangpaiyuefen'},
            {text: '7月', value: '515687', paramname: 'shangpaiyuefen'},
            {text: '8月', value: '515688', paramname: 'shangpaiyuefen'},
            {text: '9月', value: '515689', paramname: 'shangpaiyuefen'},
            {text: '10月', value: '515690', paramname: 'shangpaiyuefen'},
        ]},
        { text: '2016年', value: '2016', paramname: 'buytime', option: [
            {text: '1月', value: '515681', paramname: 'shangpaiyuefen'},
            {text: '2月', value: '515682', paramname: 'shangpaiyuefen'},
            {text: '3月', value: '515683', paramname: 'shangpaiyuefen'},
            {text: '4月', value: '515684', paramname: 'shangpaiyuefen'},
            {text: '5月', value: '515685', paramname: 'shangpaiyuefen'},
            {text: '6月', value: '515686', paramname: 'shangpaiyuefen'},
            {text: '7月', value: '515687', paramname: 'shangpaiyuefen'},
            {text: '8月', value: '515688', paramname: 'shangpaiyuefen'},
            {text: '9月', value: '515689', paramname: 'shangpaiyuefen'},
            {text: '10月', value: '515690', paramname: 'shangpaiyuefen'},
        ]},
        { text: '2017年', value: '2017', paramname: 'buytime', option: [
            {text: '1月', value: '515681', paramname: 'shangpaiyuefen'},
            {text: '2月', value: '515682', paramname: 'shangpaiyuefen'},
            {text: '3月', value: '515683', paramname: 'shangpaiyuefen'},
            {text: '4月', value: '515684', paramname: 'shangpaiyuefen'},
            {text: '5月', value: '515685', paramname: 'shangpaiyuefen'},
            {text: '6月', value: '515686', paramname: 'shangpaiyuefen'},
            {text: '7月', value: '515687', paramname: 'shangpaiyuefen'},
            {text: '8月', value: '515688', paramname: 'shangpaiyuefen'},
            {text: '9月', value: '515689', paramname: 'shangpaiyuefen'},
            {text: '10月', value: '515690', paramname: 'shangpaiyuefen'},
        ]},
        { text: '2018年', value: '2018', paramname: 'buytime', option: [
            {text: '1月', value: '515681', paramname: 'shangpaiyuefen'},
            {text: '2月', value: '515682', paramname: 'shangpaiyuefen'},
            {text: '3月', value: '515683', paramname: 'shangpaiyuefen'},
            {text: '4月', value: '515684', paramname: 'shangpaiyuefen'},
            {text: '5月', value: '515685', paramname: 'shangpaiyuefen'},
            {text: '6月', value: '515686', paramname: 'shangpaiyuefen'},
            {text: '7月', value: '515687', paramname: 'shangpaiyuefen'},
            {text: '8月', value: '515688', paramname: 'shangpaiyuefen'},
            {text: '9月', value: '515689', paramname: 'shangpaiyuefen'},
            {text: '10月', value: '515690', paramname: 'shangpaiyuefen'},
        ]}
    ]
  };
  var props = {
    data: options,
    title: '购车时间',
    value: {"children":[{"paraname":"shangpaiyuefen","text":"1月","value":"515681"}],"paraname":"buytime","text":"2017年","value":"2017"},
    placeholder: '请选择'
  }
  var pickerSelectInputInstance = (
    <div>
        <PickerSelectorInput {...props}/>
    </div>
  );
  ReactDOM.render(pickerSelectInputInstance, mountNode);
    