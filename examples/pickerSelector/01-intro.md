`<PickerSelector>`从底部弹起的可滚动选择器，现支持三种选择器，根据传入数据做不同的渲染，分别是普通选择器，二级级联选择器（时间选择器，日期选择器）、三级级联选择器（省市区选择器）。

通用props属性说明：
- ` data `: ` Object ` - 选项数据，必须设置，格式为
```
{
    paramname: 'color',
    title: '颜色',
    option: [
        { text: '黑', value: '1', paramname: 'color' },
        { text: '白', value: '2', paramname: 'color' },
        { text: '红', value: '3', paramname: 'color' },
        { text: '灰', value: '4', paramname: 'color' },
        { text: '黄', value: '5', paramname: 'color' }
    ]
};
```
- ` title `: ` String ` - Picker组件标题，会在Picker顶部中间展示;
