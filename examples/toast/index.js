'use strict';
var React = require('react');
var ReactBin = require('../ReactBin');
var utils = require('../utils');
var Markdown = utils.Markdown;
var Doc = utils.Doc;
/**样例配置区域 */
var examples = {
  basic: require('fs').readFileSync(__dirname + '/02-basic.js', 'utf-8') //测试样例
};
/**样例配置区域 */
var IconDoc = React.createClass({
  render: function() {
    return (
      <Doc>
        <h1>Toast</h1>
        <hr />
        <h2>组件介绍</h2>
        <Markdown>{require('./01-intro.md')}</Markdown>
        <h2>组件演示</h2>
        <h3>自动关闭</h3>
        <ReactBin code={examples.basic} />
      </Doc>
    );
  }
});

module.exports = IconDoc;
