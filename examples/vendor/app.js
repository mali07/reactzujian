/*!
 * app.js v1.2.5
 * Released on 11/8/2017, 7:41:09 PM
*/
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global.WBAPP = factory());
}(this, function () { 'use strict';

  var slice = Array.prototype.slice;

  /**
   * 获取数据类型
   * @param  {mix} o
   * @return {string} 返回检测数据的类型，全小写
   */
  function getType(o) {
  	if (o === null) return null;
  	if (o === undefined) return undefined;
  	return Object.prototype.toString.call(o).slice(8, -1);
  }

  function isClass(o) {
  	return Object.prototype.toString.call(o).slice(8, -1);
  }

  function isArray(o) {
  	return getType(o) === 'Array';
  }

  function isObject(o) {
  	return getType(o) === 'Object';
  }

  function isWindow(o) {
  	return getType(o) === 'Window';
  }

  function isPlainObject(obj) {
  	return isObject(obj) && !isWindow(obj) && Object.getPrototypeOf(obj) == Object.prototype;
  }

  /**
   * 扩展对象:同 Zepto $.extend
   * @param  {Object} target
   */
  function extend(target) {
  	var deep = void 0,
  	    args = slice.call(arguments, 1);

  	if (typeof target == 'boolean') {
  		deep = target;
  		target = args.shift();
  	}
  	args.forEach(function (arg) {
  		_extend(target, arg, deep);
  	});
  	return target;

  	function _extend(target, source, deep) {
  		for (var key in source) {
  			if (deep && (isPlainObject(source[key]) || isArray(source[key]))) {
  				if (isPlainObject(source[key]) && !isPlainObject(target[key])) {
  					target[key] = {};
  				}
  				if (isArray(source[key]) && !isArray(target[key])) {
  					target[key] = [];
  				}
  				_extend(target[key], source[key], deep);
  			} else if (source[key] !== undefined) {
  				target[key] = source[key];
  			}
  		}
  	}
  }

  // 获取url中参数值
  function queryParams(key) {
  	if (key === undefined) return null;
  	var search = location.search.substr(1);
  	var mReg = new RegExp('(^|&)' + key + '=([^&]*)(&|$)');
  	var mValue = search.match(mReg);
  	if (mValue != null) return unescape(mValue[2]);
  	return null;
  }

  // 获取cookie
  function getCookie(key) {
  	if (key === undefined) return undefined;
  	var ret = undefined;
  	var cookies = document.cookie;
  	var mReg = new RegExp('(^|;)\\s*' + key + '=([^;]*)(;|$)');
  	var mValue = cookies.match(mReg);

  	if (mValue != null) {
  		ret = unescape(mValue[2]);
  	}

  	if (ret !== undefined) {
  		// ret = ret[0].split('=')[1].split('; ')[0] ;
  		// app中 iOS cookie值存在问题，过滤一下
  		ret = ret.replace(/^\"|\'/i, '').replace(/\"|\'$/i, '');
  	}

  	return ret;
  }

  /**
   * 小版本中可能存在0开头的情况
   * 		所以两个比较的版本要求形式一致，严禁出现这种情况
   * 			compareVersion("08:03:10","8.2.1")
   * 		该种情况无法判定版本问题
   */
  /**
   * 比较两个版本之间大小。主要但不限于用于比较app当前版本与目标版本的大小情况
   * @param  {String} higher - 比较前期望：较高版本
   * @param  {String} lower  - 比较前期望：较低版本
   * @return {Number} result - higher>lower : 1 ; higher=lower:0 ; higher<lower:-1
   */

  /**
   * @example
   *     compareVersion('7.12.')
   */
  function compareVersion(higher, lower) {
  	var sep = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '.';

  	var higherAry = higher.split(sep),
  	    lowerAry = lower.split(sep);
  	var l = Math.max(higherAry.length, lowerAry.length);
  	var zeros = /^0+/i;

  	for (var i = 0; i < l; i++) {

  		var high = parseInt(higherAry[i] || 0);
  		var low = parseInt(lowerAry[i] || 0);

  		if (high > low) {
  			return 1;
  		}

  		if (high < low) {
  			return -1;
  		}
  	}
  	return 0;
  }

  // ----------------------------------------
  // 用于app版本获取
  // ----------------------------------------

  // 首先从 ua中获取版本号
  // 再从cookie中获取版本号
  // 最后从url中获取版本号
  //  都没有，则返回一个最大值
  function getAppVersion() {
  	var version = undefined;
  	var ua = navigator.userAgent;
  	var vReg = /W(BU|UB)A\/(\d+(.\d+){2,})/i;
  	var mValue = ua.match(vReg);

  	if (mValue != null) {
  		version = unescape(mValue[2]);
  	} else if (getCookie('cversion')) {
  		version = getCookie('cversion');
  	} else if (queryParams('cversion')) {
  		version = queryParams('cversion');
  	} else {
  		version = '999.999.999';
  	}

  	// 兼容vivo内置包
  	// app版本应为 7.1.5
  	// 实质写成了 7.4.7.0
  	if (getCookie('intermanufacturer') && compareVersion(version, '7.4.7.0') === 0) {
  		version = '7.1.5';
  	}

  	return version;
  }

  /**
   * 深复制一个对象，支持基本类型复制
   * @param  {Mixin} obj  - 复制源对象
   * @return {Mixin}      - 拷贝的新对象
   */
  function deepClone(obj) {
  	var result = void 0,
  	    oClass = getType(obj);
  	var types = [Number, String, Boolean];
  	// normalizing primitives if someone did new String('aaa'), or new Number('444');
  	//一些通过new方式建立的东东可能会类型发生变化，我们在这里要做一下正常化处理
  	//比如new String('aaa'), or new Number('444')
  	for (var i = 0; i < types.lenght; i++) {
  		if (obj instanceof types[i]) {
  			result = types[i](obj);
  			break;
  		}
  	}
  	//确定result的类型
  	if (oClass === 'Object') {
  		result = {};
  	} else if (oClass === 'Array') {
  		result = [];
  	} else {
  		return obj;
  	}
  	for (var key in obj) {
  		var copy = obj[key];
  		if (getType(copy) == 'Object') {
  			result[key] = deepClone(copy); //递归调用
  		} else if (getType(copy) == 'Array') {
  			result[key] = deepClone(copy);
  		} else {
  			result[key] = obj[key];
  		}
  	}
  	return result;
  }

  /**
   * 优化回调函数
   * @param  {Function|String} func    - 回调函数
   * @param  {Object} context          - 回调函数的执行上下文
   * @return {Function}                - 包装过的回调函数
   */
  function optimizeCb(func) {
  	var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

  	var defaultFunc = function defaultFunc() {
  		console.log('callback does not exist');
  	};
  	if (getType(func) === 'String') {
  		try {
  			var ary = func.split('.');
  			func = window;
  			for (var i = 0; i < ary.length; i++) {
  				func = func[ary[i]];
  			}
  		} catch (e) {
  			func = defaultFunc;
  		}
  	}

  	if (getType(func) !== 'Function') {
  		func = defaultFunc;
  	}

  	if (context === void 0) {
  		return func;
  	}

  	return function () {
  		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
  			args[_key] = arguments[_key];
  		}

  		return func.apply(context, args);
  	};
  }

  /**
   * 枚举数据类型
   * @type {Array}
   */
  var TYPE_DATA = ['Boolean', 'Number', 'String', 'Function', 'Array', 'Date', 'RegExp', 'Object', 'Error'];

  /**
   * app版本
   * 通过util中的方法获取到，作为静态变量缓存
   */
  var APP_VERSION = getAppVersion();

  /**
   * 是否在app中：严格模式
   * 即 userAgent 中包含 WUBA 字样时
   * @type {Boolean}
   */
  var IS_IN_APP_STRICT = function () {
    return (/W(UB|BU)A/i.test(navigator.userAgent)
    );
  }();

  /**
   * 是否在app中
   * 1： userAgent
   * 2: cookie
   */
  var IS_IN_APP = function () {
    /* istanbul ignore if */
    if (IS_IN_APP_STRICT) {
      return true;
    }
    return getCookie('58ua') === '58app';
  }();

  /**
   * 操作系统 ios || android
   */
  var OS = navigator.userAgent.indexOf('Android') > -1 ? 'android' : 'ios';

  var isBelow = compareVersion('7.6.0', APP_VERSION) >= 0;

  function weblog(actionName, params) {
      /* istanbul ignore if */
      if (OS === 'ios' && isBelow) {
          ['page_type', 'action_type'].forEach(function (key) {
              var value = params[key];
              var aKey = key.split('_').join('');
              params[aKey] = value;
              // params[key] = undefined ;
          });
      }

      return {
          params: params
      };
  }

  function pagetrans(actionName, params) {
      var isVendor = getCookie('intermanufacturer') ? true : false;
      var isErrorVersion = compareVersion(APP_VERSION, '7.1.5') === 0;
      /* istanbul ignore if */
      if (isVendor && isErrorVersion) {
          if (params.pagetype && params.pagetype === 'common') {
              params.pagetype = 'link';
          }

          if (params.content && params.content.pagetype && params.content.pagetype === 'common') {
              params.content.pagetype = 'link';
          }
      }

      return { params: params };
  }

  function info_birthday_picker(actionName, params, cb) {
  	var defaultValue = params.default_value;
  	if (defaultValue) {
  		defaultValue = defaultValue.replace(/-/g, '');
  	}

  	return {
  		actionName: 'date_picker',
  		params: {
  			default_value: {
  				birthday: defaultValue
  			}
  		},
  		callback: function callback(response) {
  			var rType = getType(response);
  			var res = response;
  			if (rType === undefined) {
  				return;
  			}
  			if (rType === 'String') {
  				res = JSON.parse(response);
  			}
  			var state = res.state.toString();
  			var data = '';
  			try {
  				data = res.data.birthday;
  				data = data.substring(0, 4) + '-' + data.substring(4, 6) + '-' + data.substring(6, 8);
  			} catch (e) {
  				data = res.data;
  			}

  			cb && optimizeCb(cb)(JSON.stringify({
  				state: state,
  				data: data
  			}));
  		}
  	};
  }

  function get_pay(actionName, params) {
    // 新增参数：accountType & balanceType
    // accountType:业务线
    // babanceType:支付流程 100：老流程 ；200：新流程
    /**
     * 针对新参数处理：
     * 如果accountType无值，则将balanceType值置为 100
     * balanceType的值默认为100，即老流程
     */
    var _params$balanceType = params.balanceType,
        balanceType = _params$balanceType === undefined ? '100' : _params$balanceType,
        accountType = params.accountType;

    if (!accountType && accountType != 0) {
      balanceType = '100';
    }
    params['balanceType'] = balanceType;

    return { params: params };
  }

  function get_user_info(actionName) {
      var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var cb = arguments[2];

      var key = params.key;
      var wrapCb = void 0;
      /* istanbul ignore if */
      if (OS === 'android' && cb) {
          wrapCb = function patchWrapCb(response) {
              var rcb = optimizeCb(cb);
              var resp = response;
              /**
              * 返回的数据大概为这样的情况：
              *   params={"xxkey":"xxValue"}
              *
              * 正常应为:
              *   params=`${encodeURIComponent(JSON.stringify({xxKey:xxValue}))}` ;
              */
              if (key === 'jumpinfo' && response && response.indexOf('params={') >= 0) {
                  try {
                      var pat = /params=(.*)/;
                      resp = 'params=' + encodeURIComponent(response.match(pat)[1]);
                  } catch (e) {
                      console.log(e);
                  }
              } else if (!key && response && response.indexOf('"jumpinfo":') >= 0) {
                  /**
                   * 数据异常基本同key=jumpinfo时
                   * 处理逻辑为：
                   *  先从字符串中取出encode后，
                   *  再插入字符串中
                   */
                  var isParseError = false;
                  try {
                      JSON.parse(response);
                  } catch (e) {
                      isParseError = true;
                  }

                  if (isParseError) {
                      try {
                          var _pat = /\"jumpinfo\":\"params=(\{.*\})\"/;
                          var mValue = response.match(_pat);
                          var rValue = encodeURIComponent(mValue[1]);
                          resp = response.replace(_pat, '"jumpinfo":"params=' + rValue + '"');
                          // 再尝试一次，以保证正确
                          JSON.parse(resp);
                      } catch (e) {
                          console.log(e);
                      }
                  }
              } else {
                  resp = response;
              }

              rcb(resp);
          };
      } else {
          wrapCb = cb;
      }

      return {
          params: params,
          callback: wrapCb
      };
  }



  var patches = Object.freeze({
  	weblog: weblog,
  	pagetrans: pagetrans,
  	info_birthday_picker: info_birthday_picker,
  	get_pay: get_pay,
  	get_user_info: get_user_info
  });

  var _CUSTOM_PROTOCOL_SCHEME = 'nativechannel://';
  var _CALLBACK_NAMESPACE = '__WubaJSBridge_Callback__';
  var _NOOP = function _NOOP() {};
  var doc = document;
  var win = window;
  var devtools = win['__WBAPP_DEVTOOLS_GLOBAL_HOOK__'];
  var _nativePrompt = win.prompt;
  var _sendMessageQueue = [];
  var _iOSMessageFrameQueue = [];
  var _os = OS;
  var _appVersion = APP_VERSION;
  var _isInApp = IS_IN_APP_STRICT;

  var _callback_unique_id = 0;
  var _isJSBridgeReady = false;
  var _bridgeNative = _NOOP;
  var WubaJSBridge = {
      debug: false
  };

  // 初始化jsbridge
  function _initJSBridge() {
      var onReadyBridge = function onReadyBridge() {
          document.removeEventListener('DOMContentLoaded', onReadyBridge);
          /* istanbul ignore if */
          if (_isJSBridgeReady) {
              return;
          }
          _isJSBridgeReady = true;
          _fetchQueue();
      };

      win.prompt = function () {
          console.warn('使用该SDK不建议使用prompt方法');
      };

      // 初始化bridge方法
      /* istanbul ignore next */
      _bridgeNative = function () {
          if (!_isInApp) {
              return _NOOP;
          }
          if (_os === 'android' && !(win.stub && win.stub.jsCallMethod)) {
              return _bridgeAndroid;
          }
          if (_os === 'ios') {
              return _bridgeIos;
          }
          return _NOOP;
      }();

      /* istanbul ignore if */
      if (/complete|loaded|interactive/.test(doc.readyState) && doc.body) {
          onReadyBridge();
          return;
      }

      doc.addEventListener('DOMContentLoaded', onReadyBridge, false);
  }

  // 完整参数:actionName, params , callback , errorCallback
  //  params , callback ,errorCallback 参数可选且 params参数可缺省
  //  正确的参数形式：
  //      1. (actionName)
  //      2. (actionName,params)
  //      3. (actionName,params,callback)
  //      4. (actionName,params,callback , errorCallback)
  //      5. (actionName,callback)
  //  其他参数形式均不正确
  //
  //  参数类型:
  //      actionName:{String}
  //      params:{Object}
  //      callback:{Function || String}
  //      errorCallback:{String}
  //
  //  备注：
  //      callback 为String类型时，必须为全局函数的函数名，否则不能正确的调用方法
  function _call(actionName) {
      if (!actionName) {
          return;
      }
      var params = (arguments.length <= 1 ? undefined : arguments[1]) || {};
      var cb = arguments.length <= 2 ? undefined : arguments[2];
      var paramsType = getType(params);
      var message = void 0;
      var patcher = patches[actionName];
      var patch = null;

      // 处理传入参数问题
      // 此处处理使得 参数形式 5 为正确参数形式
      if ((arguments.length <= 1 ? 0 : arguments.length - 1) === 1 && (paramsType === 'Function' || paramsType === 'String')) {
          cb = params;
          params = {};
      }
      params = extend(true, {}, params);
      // patcher
      if (getType(patcher) === 'Function') {
          patch = extend({
              pass: true,
              actionName: actionName,
              callback: cb
          }, patcher.call(patcher, actionName, params, cb));

          /* istanbul ignore if */
          if (!patch.pass) {
              return;
          }
          params = patch.params || params;
          actionName = patch.actionName;
          cb = patch.callback;
      }

      // 处理回调函数问题
      if (cb) {
          params['callback'] = _callback(cb);
      }

      message = _packMessage(actionName, params);
      _sendMessage(message);
      // 加埋点
      // 该方案会产生大量数据，暂不执行
      // _track(actionName,params);
      return message;
  }
  // 打包消息成Native需要的形式
  function _packMessage(actionName, params) {
      var message = '' + _CUSTOM_PROTOCOL_SCHEME + actionName;
      if(actionName == 'selectdata'&&params){
          message += '?paras=' + encodeURIComponent(JSON.stringify(params));
      }else if(params) {
          message += '?params=' + encodeURIComponent(JSON.stringify(params));
      }
      return message;
  }

  // 发送消息到队列中
  function _sendMessage(message) {
      _sendMessageQueue.push(message);
      _fetchQueue();
  }

  // 请求(处理)消息队列
  function _fetchQueue() {
      if (!_isJSBridgeReady || _sendMessageQueue.length <= 0) {
          return;
      }
      var message = _sendMessageQueue.shift();
      _bridgeNative(message);
      _fetchQueue();
      win.WubaJSBridge && win.WubaJSBridge.debug && console.info('->Native: %s', decodeURIComponent(message));
      devtools && devtools.inject && devtools.inject(message);
  }

  /**
   * 用于制造回调函数
   * @param  {Function} fn   - 真正的回调函数
   * @param  {String}   name - 回调函数挂载安装成 CALLBACK_NAMESPACE[name]的形式，可选参数
   * @param  {mixin}   context - 回调的执行上下文
   * @param  {Boolean} execOnce -
   * @return {String}        - 字符串，用于传给native的回调函数名。如：WubaJSBridgeNativeInvokeCallback.JS_CALLBACK_1 ;
   */
  function _callback(cb) {
      var name = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'JS_CALLBACK_' + ++_callback_unique_id;

      // 先判断命名空间是否健在 ， 如有损坏则修复
      if (getType(win[_CALLBACK_NAMESPACE]) !== 'Function') {
          win[_CALLBACK_NAMESPACE] = _NOOP;
      }
      var cbType = getType(cb);
      if (cbType !== 'String' && cbType !== 'Function') {
          return;
      }

      win[_CALLBACK_NAMESPACE][name] = _wrapCb(cb);

      return _CALLBACK_NAMESPACE + '.' + name;
  }

  // 处理回调函数参数类型不规范的错误
  function _wrapCb(cb) {
      return function wrappedCb() {
          for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
              args[_key] = arguments[_key];
          }

          var optimizeArgs = args.map(function (arg) {
              if (getType(arg) === 'Number') {
                  return arg.toString();
              }
              // json
              if (getType(arg) === 'Object') {
                  arg = JSON.stringify(arg);
              }
              // json字符串
              if (getType(arg) === 'String' && arg.match(/^{.*}$/)) {
                  // 如果json字符串中有state或status参数
                  // 则该字段值强制转化为string类型
                  ['state', 'status'].forEach(function (key) {
                      var reg = new RegExp('\"' + key + '\"\\s*:\s*(\\d+)', 'g');
                      arg = arg.replace(reg, '"' + key + '":"$1"');
                  });
              }
              return arg;
          });
          var fn = optimizeCb(cb, window);
          return fn.apply(null, optimizeArgs);
      };
  }

  // 是否复用？
  // 复用可能有问题
  // 同时请求的问题
  /* istanbul ignore next */
  function _getIframe() {
      if (_iOSMessageFrameQueue.length > 0) {
          return _iOSMessageFrameQueue.shift();
      }
      var ifr = void 0;
      ifr = doc.createElement('iframe');
      ifr.setAttribute('style', 'display:none;');
      ifr.setAttribute('height', '0px');
      ifr.setAttribute('width', '0px');
      ifr.setAttribute('frameborder', '0');
      doc.body.appendChild(ifr);
      return ifr;
  }

  // ios交互
  /* istanbul ignore next */
  function _bridgeIos(message) {
      var ifr = _getIframe();
      ifr.src = message;
      // recycle
      setTimeout(function () {
          _iOSMessageFrameQueue.push(ifr);
      }, 20);
  }

  // android交互
  /* istanbul ignore next */
  function _bridgeAndroid(message) {
      _nativePrompt(message);
  }

  WubaJSBridge = extend(WubaJSBridge, {
      invoke: _call,
      call: _call,
      callback: _callback,
      _fetchQueue: _fetchQueue
  });

  _initJSBridge();

  var WubaJSBridge$1 = WubaJSBridge;

  function setTitle(title) {
  	WubaJSBridge$1.invoke('set_title', {
  		title: title
  	});
  }

  function goback() {
  	var isBacktoroot = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

  	WubaJSBridge$1.invoke('goback', {
  		is_backtoroot: isBacktoroot
  	});
  }

  function dialog(type) {
  	var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  	var callback = arguments[2];


  	var _params = config;
  	_params.type = type;
  	WubaJSBridge$1.invoke('dialog', _params, callback);
  }

  function toast(msg) {
  	msg = msg + '';
  	WubaJSBridge$1.invoke('toast', {
  		msg: msg
  	});
  }

  function getPosition(callback) {
  	WubaJSBridge$1.invoke('get_position', callback);
  }

  function setWeblog(pageType, actionType, opts) {
  	// 参数处理
  	var params = {
  		page_type: pageType,
  		action_type: actionType
  	};

  	if (getType(opts) === 'Object') {
  		params = extend(params, opts);
  	}

  	if (params.params) {
  		params.params = params.params.join(',').split(',');
  	}

  	WubaJSBridge$1.invoke('weblog', params);
  }

  function reload() {
  	WubaJSBridge$1.invoke('reload');
  }

  function installApp(url) {
  	var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'browser';

  	WubaJSBridge$1.invoke('install_app', {
  		url: url,
  		type: type
  	});
  }

  function isInstallApp(params, callback) {
  	// 参数校验
  	WubaJSBridge$1.invoke('is_install_app', params, callback);
  }

  function openApp(params) {
  	// 参数校验
  	WubaJSBridge$1.invoke('open_app', params);
  }

  function pageFinish() {
  	WubaJSBridge$1.invoke('page_finish');
  }

  function getUserInfo() {
  	var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  	var callback = arguments[1];


  	WubaJSBridge$1.invoke('get_user_info', {
  		key: key
  	}, callback);
  }

  function setAlarm(config) {
      WubaJSBridge$1.invoke('set_alarm', config);
  }

  /**
   * 取消已设置的提醒
   * @param  {String} n 提醒消息的唯一标识
   */
  function cancelAlarm(n) {
  	WubaJSBridge$1.invoke('cancel_alarm', {
  		n: n
  	});
  }

  function imgPreview(imgs) {
  	var current = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  	var txts = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];


  	if (getType(imgs) !== 'Array') {
  		return;
  	}

  	WubaJSBridge$1.invoke('show_img', {
  		url_arr: imgs.join(','),
  		txt_arr: txts.join(','),
  		index: current
  	});
  }

  /**
   * 适配页面url的协议头
   * @param  {String}  url     - 原始url
   * @param  {Boolean} isForce - 是否开启强制适配，默认 true ,开启强制适配。强制适配会删除原来url中自带的协议头，适配当前页面的协议头。
   * @return {String}          - 适配后的url
   */

  function adapterProtocol(url, isForce) {
      if (getType(url) !== 'String') {
          return url;
      }
      isForce = isForce === false ? false : true;
      var protocol = window.location.protocol == 'https:' ? 'https:' : 'http:';
      var regProtocol = isForce === true ? /^(http:|https:)*\/\//i : /^\/\//i;
      url = url.replace(regProtocol, protocol + '//');

      return url;
  }

  /**
   * 支持的url格式：
   * 		(1) http://xxx.xxx.xx/
   * 	 	(2) https://xxx.xxx.xx/
   * 	  	(3) //xxx.xxx.xx
   * 强制适配时：
   * 		3种url均会返回适配当前协议的url。
   * 不强制适配时:
   * 		第三种url会适配，一二种url会按原样返回。
   */

  /**
   * 创建新跳转协议的方法
   * @param  {Object} params  - 同pagetrans Action参数
   * @return {String}        - 新跳转协议的URI ,当参数不对时返回 undefined
   */
  function createURI(params) {
  	if (getType(params) !== 'Object' || getType(params.content) !== 'Object') {
  		return undefined;
  	}

  	var uri = 'wbmain://jump/';
  	var tradeLine = params.tradeline;
  	var content = params.content;

  	var pagetype = content.pagetype;

  	// tradeLine & pagetype 为必填选项
  	if (getType(tradeLine) !== 'String' && getType(pagetype) !== 'String') {
  		return undefined;
  	}

  	uri += tradeLine + '/' + pagetype + '?params=${params}';

  	var externalParams = extend(true, {}, {
  		isFinish: false,
  		isBackToMain: false,
  		needLogin: false,
  		isNoAnimated: false,
  		isSlideinBottom: false
  	}, {
  		isFinish: content.isfinish || content.isFinish || content.is_finish,
  		isBackToMain: content.is_backtomain || content.isBacktomain || content.isbacktomain,
  		needLogin: content.needlogin || content.needLogin || content.need_login,
  		ABMark: content.ABMark,
  		isNoAnimated: content.isNoAnimated,
  		isSlideinBottom: content.isSlideinBottom
  	});

  	for (var key in externalParams) {
  		// content[key] = undefined ;
  		var value = externalParams[key];
  		if (getType(value) !== undefined && getType(value) !== null) {
  			uri += '&' + key + '=' + value;
  		}
  	}

  	// content.pagetype = undefined ;

  	uri = uri.replace('${params}', encodeURIComponent(JSON.stringify(content)));

  	return uri;
  }

  // 从 7.4.0版本开始使用新跳转协议
  // const IS_HIGHER_VERSION = compareVersion(getAppVersion(),'7.4.0')>=0?true:false ;

  var keys = [{
  	from: 'cate_id',
  	to: 'cateid'
  }, {
  	from: 'is_finish',
  	to: 'isfinish'
  }, {
  	from: 'meta_params',
  	to: 'params'
  }, {
  	from: 'filter_params',
  	to: 'filterParams'
  }, {
  	from: 'info_id',
  	to: 'infoID'
  }, {
  	from: 'is_backtoroot',
  	to: 'backtoroot'
  }, {
  	from: 'is_showpub',
  	to: 'showpub'
  }, {
  	from: 'is_showswift',
  	to: 'showswift'
  }, {
  	from: 'is_showarea',
  	to: 'showarea'
  }, {
  	from: 'recommoned_info',
  	to: 'recomInfo'
  }, {
  	from: 'loading_type',
  	to: 'loadingtype'
  }];

  /**
   * 用于矫正key值
   * @param  {Object} object - 需要矫正key的对象
   * @param  {String} from   - 需要矫正的key
   * @param  {String} to     - 矫正后的key
   * @return {Object}        - 矫正后的对象
   */
  function rectifyKey(object, from, to) {
  	var rectifyValue = object[from];
  	object[to] = rectifyValue;
  	object[from] = undefined;
  	delete object[from];
  	return object;
  }

  function pagetrans$1() {
  	var tradeline = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'core';
  	var content = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};


  	// content字符串规范化兼容处理
  	for (var i = 0, key; key = keys[i++];) {
  		if (content['' + key['from']] !== undefined) {
  			rectifyKey(content, key['from'], key['to']);
  		}
  	}

  	// url & meta_url & data_url 适配https
  	['url', 'meta_url', 'data_url'].forEach(function (key) {
  		var url = content[key];
  		if (getType(url) === 'String' && url !== '') {
  			content[key] = adapterProtocol(url, false);
  		}
  	});

  	var param = {
  		action: 'pagetrans',
  		tradeline: tradeline,
  		content: content
  	};

  	// 暂不做支持
  	// if(IS_HIGHER_VERSION){
  	// 	WubaJSBridge.invoke('pagetrans',{
  	// 		protocol:createURI(param)
  	// 	}) ;
  	//
  	// 	return ;
  	// }


  	//执行通信
  	WubaJSBridge$1.invoke('pagetrans', param);
  }

  function initSearch(cateId, listName, cate, placeholder, defaultValue) {
  	WubaJSBridge$1.invoke('init_search', {
  		cate_id: cateId,
  		list_name: listName,
  		cate: cate,
  		placeholder: placeholder,
  		default_value: defaultValue
  	});
  }

  // 序列化字符串形式的函数名
  // 整理成刑如：[nameSpaceMain.][nameSpaceSub.][ ... .]functionName 的字符串
  // Test
  // 'window["fn1"]' => 'fn1'
  // 'window.fn' => 'fn'
  // 'play.feng["callback"]' => play.feng.callback
  function serializeStringFn(fn) {
  	var _dropWindow = /^(window(\.|\[('|")))/i;
  	var _splitRE = /\.|\[['|"]|['|"]\]\.{0,1}/ig;

  	if (fn === undefined || getType(fn) !== 'String') {
  		return undefined;
  	}
  	var _ary = fn.replace(_dropWindow, '').split(_splitRE);
  	var ret = _ary.join('.').replace(/\.{2,}/ig, '.').replace(/\.+$/ig, '');

  	return ret;
  }

  // 执行回调
  function execWrapCallback(fn) {
  	if (fn == undefined) {
  		return false;
  	}
  	var _type = getType(fn);
  	var _serilize;
  	var _fn = window;

  	for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
  		args[_key - 1] = arguments[_key];
  	}

  	if (_type === 'Function') {
  		fn.apply(this, args);
  	} else if (_type === 'String') {
  		_serilize = serializeStringFn(fn).split('.');
  		for (var i = 0, l = _serilize.length; i < l; i++) {
  			// 当fn不存在时会报错
  			_fn = _fn[_serilize[i]];
  		}
  		_fn.apply(this, args);
  	} else {
  		return false;
  	}

  	return true;
  }

  // ---------------------------------
  /*
  * 环境设置
  */
  // ---------------------------------

  // set-env-start-123

  var ENV = 'production';

  // import Event from '../events/Event.js' ;
  // import getType from '../utils/getType.js' ;


  /**
   * 监听Native事件
   * @function
   * @param  {String}    eventName  - Native事件名称，可选值 goback/pageshow
   * @param  {Function}  handler   - 回调函数
   * @param  {Boolean}   throwError - 回调函数执行错误是否可直接返回，默认true
   * @description throwError仅针对eventName="goback"时有效。生产环境强制为true。
   */
  function deviceEvent(eventName, handler, throwError) {
  	var cb = handler;

  	// var throwError = true ;
  	if (throwError === undefined || ENV === 'production') {
  		throwError = true;
  	}

  	// goback事件时
  	if (eventName == 'goback' && throwError) {
  		cb = function cb() {
  			try {
  				execWrapCallback(handler);
  			} catch (e) {
  				WubaJSBridge$1.invoke('goback', {});
  			}
  		};
  	}

  	WubaJSBridge$1.invoke('device_event', {
  		type: eventName
  	}, cb);
  }

  /**
   * 扩展右上角按钮
   * @function
   * @author chenqiang01
   * @param  {Array}   btnGroup - 按钮配置信息
   * @param  {btnsCallback} callback - 回调函数
   * @param  {String}  type - 按钮类型:default vertical ，可选 horizontal/vertical
   * @param  {String}   cmd      - 命令，目前仅支持 init，其他值按照init执行
   * @description - btnGroup为空数组时，不展示按钮(已展示按钮会消除)
   * @example
   * extendBtn([{
   * 	key:'share' ,
   * 	txt:'分享' ,
   * 	icon:'share'
   * }],function(key,index){
   * 	// todo
   * }) ;
   */
  function extendBtn(btnGroup, callback) {
    var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'vertical';
    var cmd = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'init';

    WubaJSBridge$1.invoke('extend_btn', {
      cmd: cmd,
      type: type,
      config: btnGroup
    }, callback);
  }

  /**
   * 回调函数可为Function或String类型，为String类型时必须是全局函数的函数名
   * @callback btnsCallback
   * @param {String} key - 按钮标识key，对应btnGroup中key值
   * @param {String} index - 按钮在按钮组中的下标，从"0"开始
   */

  function loadingbar(cmd) {
  	var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '1';

  	WubaJSBridge$1.invoke('loadingbar', {
  		type: type,
  		cmd: cmd
  	});
  }

  function openQrscan(callback) {
  	WubaJSBridge$1.invoke('qrscan', callback);
  }

  function share(config, callback) {
  	WubaJSBridge$1.invoke('share', {
  		config: config
  	}, callback);
  }

  function login(callback) {
    WubaJSBridge$1.invoke('login', callback);
  }

  function logout(callback) {
    WubaJSBridge$1.invoke('logout', callback);
  }

  function isLogin(callback) {
  	WubaJSBridge$1.invoke('is_login', callback);
  }

  var _gestureState = 'on';

  function toggleGesture(cmd) {
  	_gestureState = _gestureState == 'on' ? 'off' : 'on';

  	if (cmd !== undefined) {
  		_gestureState = cmd;
  	}

  	WubaJSBridge$1.invoke('toggle_gesture', {
  		cmd: _gestureState
  	});
  }

  function imgUpload(cateId, listName, callback, opts) {
  	var params = {
  		cate_id: cateId,
  		list_name: listName
  	};

  	if (opts) {
  		params = extend(params, opts);
  	}

  	params.source_type = '0';
  	WubaJSBridge$1.invoke('upload_img', params, callback);
  }

  function bindAccount(type, callback) {
  	WubaJSBridge$1.invoke('bind_account', {
  		type: type
  	}, callback);
  }

  function isBindAccount(type, callback) {
  	WubaJSBridge$1.invoke('is_bind_account', {
  		type: type
  	}, callback);
  }

  // 兼容插件对外暴露
  // import {Adapter} from 'shared/adapter' ;

  var actions = {
      setTitle: setTitle,
      goback: goback,
      dialog: dialog,
      toast: toast,
      getPosition: getPosition,
      setWeblog: setWeblog,
      setWebLog: setWeblog,
      reload: reload,
      installApp: installApp,
      isInstallApp: isInstallApp,
      openApp: openApp,
      pageFinish: pageFinish,
      getUserInfo: getUserInfo,
      setAlarm: setAlarm,
      cancelAlarm: cancelAlarm,
      imgPreview: imgPreview,
      pagetrans: pagetrans$1,
      initSearch: initSearch,
      deviceEvent: deviceEvent,
      extendBtn: extendBtn,
      loadingbar: loadingbar,
      openQrscan: openQrscan,
      share: share,
      login: login,
      logout: logout,
      isLogin: isLogin,
      toggleGesture: toggleGesture,
      imgUpload: imgUpload,
      bindAccount: bindAccount,
      isBindAccount: isBindAccount
  };

  // -------------------------
  /**
   * 用于参数扩展：用户输入参数覆盖默认参数
   */
  // -------------------------

  /**
   * 增加或对象属性或修改对象属性值
   * @param  {Object} from - 可添加或修改原始对象的数据对象
   * @param  {Object} to   - 原始对象
   * @return {Object}      - 两个参数对象合并后的对象
   * @description - 参数值merge `from` 对象数据，即当两个对象(`from` / ` to`)中同时存在某个键值对时，以`from` 对象为准 ;
   *              该方法会修改 `to`对象
   */
  function inherit(from, to) {
  	for (var key in from) {

  		var newValue = from[key];
  		var type = getType(newValue);

  		if (type === 'Object') {
  			if (to[key] == undefined) {
  				to[key] = {};
  			}
  			to[key] = inherit(newValue, to[key]);
  			continue;
  		}

  		if (type) {
  			to[key] = from[key];
  		}
  	}

  	return to;
  }

  /*
  * single
  *   params:{
          title:"title" ,
          content:"content" ,
          btn_txt:"text"
      } ;
  */
  function single() {
  	var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  	var callback = arguments[1];


  	if (getType(params) === 'Function') {
  		callback = params;
  		params = {};
  	}

  	var _params = {
  		title: '提示',
  		content: '',
  		first_txt: '确定'
  	};

  	inherit(params, _params);

  	dialog('single', _params, callback);
  }

  /*
  * double
  	params = {
  		title: "",
  		content: "",
  		firsttxt: "",
  		secondtxt: "",
  	}
  */
  function double() {
  	var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  	var callback = arguments[1];

  	if (getType(params) === 'Function') {
  		callback = params;
  		params = {};
  	}
  	var _params = {
  		title: '提示',
  		content: '',
  		fisrt_txt: '确定',
  		second_txt: '取消'
  	};

  	inherit(params, _params);

  	dialog('double', _params, callback);
  }

  /*
  * confim
  */
  function confirm(msg, callback, title) {
  	var _params = {
  		title: title || window.location.host + '提示',
  		content: msg || '',
  		first_txt: '确定',
  		second_txt: '取消'
  	};

  	dialog('double', _params, callback);
  }

  /*
  * alert
  */

  function alert(msg, title) {
  	var _params = {
  		title: title || window.location.host + '提示',
  		content: msg || '',
  		first_txt: '确定'
  	};

  	dialog('single', _params);
  }

  var dialog$1 = {
  	single: single,
  	double: double,
  	confirm: confirm,
  	alert: alert
  };

  function to() {
  	var shareto = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'WEIXIN,FRIENDS';
  	var opts = arguments[1];
  	var callback = arguments[2];

  	if (getType(opts) !== 'Object') {
  		return;
  	}

  	var _sharetoType = getType(shareto);

  	var _params = {},
  	    _sharetoAry = [],
  	    _shareConfig = [],
  	    _itemInfo = {};

  	if (_sharetoType === 'Array') {
  		_sharetoAry = shareto;
  	} else if (_sharetoType === 'String') {
  		_sharetoAry = shareto.split(',');
  	} else {
  		_sharetoAry = ['WEIXIN', 'FRIENDS'];
  	}

  	for (var i = 0; i < _sharetoAry.length; i++) {
  		_itemInfo = {};
  		_itemInfo.shareto = _sharetoAry[i];
  		for (var key in opts) {
  			_itemInfo[key] = opts[key];
  		}

  		_shareConfig.push(_itemInfo);
  	}

  	share(_shareConfig, callback);
  }

  function info() {
  	var shareto = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'WEIXIN,FRIENDS';
  	var opts = arguments[1];
  	var callback = arguments[2];

  	if (getType(opts) !== 'Object') {
  		return;
  	}
  	opts.type = undefined;
  	opts.data_url = undefined;
  	// opts.imgurl = opts.thumburl ;
  	to(shareto, opts, callback);
  }

  function img() {
  	var shareto = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'WEIXIN,FRIENDS';
  	var opts = arguments[1];
  	var callback = arguments[2];

  	if (getType(opts) !== 'Object') {
  		return;
  	}
  	opts.type = 'imgshare';
  	to(shareto, opts, callback);
  }

  var share$1 = {
  	to: to,
  	info: info,
  	img: img
  };

  // -----------------------------------
  // 空值时不替换的需求
  // -----------------------------------

  /**
   * 跳转Native列表页
   * @function
   * @param  {String} tradeLine - 业务线名称
   * @param  {String} listName  - 分类名称
   * @param  {String} cateId    - 分类ID
   * @param  {String} localName - 城市简称
   * @param  {String} title     - 页面标题
   * @param  {Object} opts      - 其他配置项
   * @return {undefined}        - 正常执行无返回
   * @description - @param opts 结构为
   * opts = {
   * 	is_backtomain:false , // {Boolean} default:false - 外部调起时是否直接返回首页
   * 	is_finish:false ,// {Boolean} default false -是否直接返回跳转来源页面
   * 	meta_url:'' ,// {String} - 数据来源url
   * 	meta_params:'' , // {Object} - 请求meta的附带参数
   * 	filter_params:'' // {Object} - Native列表页面的筛选条件 *
   * }
   */
  var loadNativeList = function loadNativeList(tradeLine, listName, cateId, localName, title, opts) {

  	var metaUrl = location.host.indexOf('test') != -1 ? '//apptest.58.com/api/list' : '//app.58.com/api/list';

  	var _content = {
  		pagetype: 'list',
  		list_name: listName,
  		cateid: cateId,
  		local_name: localName,
  		title: title,
  		is_backtomain: false,
  		isfinish: false,
  		meta_url: metaUrl,
  		meta_params: undefined,
  		filter_params: undefined
  	};

  	inherit(opts, _content);
  	// _content = Object.assign(_content,opts) ;

  	pagetrans$1(tradeLine, _content);
  };

  /**
   * 跳转Native详情页
   * @param  {String} tradeLine - 业务线名称
   * @param  {String} listName  - 分类名称
   * @param  {String} infoId    - 帖子ID
   * @param  {String} localName - 城市简称
   * @param  {String} fullPath  - 埋点用
   * @param  {Object} opts      - 其他可选填配置项
   * @return {undefined}        - 正常调用无回调
   * @description - @param opts 完整参数：
   * opts = {
   * 	is_backtomain:false ,// {Boolean} - 是否直接返回app首页   default false
   * 	charge_url:'' , // {Boolean} -计费url , 用于精准计费
   * 	filter_params:{} , // {Object} - 如果详情页的推荐信息需要用到筛选条件，需要将filterParams 传入
   * 	pre_info :"" , // - Android是Xml(转义）, iOS是JSON
   * }
   */
  var loadNativeDetail = function loadNativeDetail(tradeLine, listName, infoId, localName, fullPath, opts) {

  	var dataUrl = listName == 'appxiaoqu' ? '//app.58.com/dict/detail' : '//app.58.com/api/detail';

  	var _content = {
  		pagetype: 'detail',
  		title: '详情',
  		list_name: listName,
  		info_id: infoId,
  		local_name: localName,
  		full_path: fullPath,
  		data_url: dataUrl,
  		is_backtomain: false,
  		recomInfo: false, //是否为推荐信息
  		use_cache: true, //是否使用缓
  		charge_url: undefined,
  		filter_params: undefined,
  		common_params: undefined, //选填项，业务线透传参数用,
  		pre_info: undefined // Android是Xml(转意）, Ios是json
  	};

  	inherit(opts, _content);
  	// _content = Object.assign(_content,opts) ;

  	pagetrans$1(tradeLine, _content);
  };

  /**
   * 跳转Web页面
   * @param  {String} url  - 跳转的web页面链接地址
   * @param  {String} title - 跳转的web页面显示标题
   * @param  {Object} opts - 跳转web页面的其他可选配置项
   * @return {undefined}
   * @description - @param opts 完整参数：
   * opts = {
   *
   * }
   */
  var loadWebview = function loadWebview(url, title, opts) {
  	var _content = {
  		pagetype: 'common',
  		url: url,
  		title: title
  	};

  	inherit(opts, _content);
  	// _content = Object.assign(_content,opts) ;

  	pagetrans$1('core', _content);
  };

  var pagetrans$2 = {
  	loadNativeList: loadNativeList,
  	loadNativeDetail: loadNativeDetail,
  	loadWebview: loadWebview
  };

  // import loadingbar from './loadingbar.extend' ;
  // import extendBtn from './extendbtn.extend' ;
  // import downApp from './downapp.extend' ;

  var extendFuncs = {
      dialog: dialog$1,
      share: share$1,
      pagetrans: pagetrans$2
  };

  var commonHeader = {
  	isReady: false
  };

  function getCookieMap() {
  	var cookies = document.cookie.split(/;\s*/ig);
  	for (var i = 0, cookie; cookie = cookies[i++];) {
  		var tmpAry = cookie.split('=');
  		var value = tmpAry[1] || '';

  		appendHeader(tmpAry[0], value);
  	}
  }

  function appendHeader(key, value) {
  	var illegalValueReg = /(^\"|\')|(\"|\'$)/ig;
  	var trimReg = /(^\s*)|(\s$)/ig;
  	var nameReg = /_|-/ig;

  	key = key.replace(trimReg, '').replace(nameReg, '').toLowerCase();
  	value = value.replace(illegalValueReg, '').replace(trimReg, '');
  	commonHeader[key] = value;
  }

  function JSBridgeReady(callback) {

  	getCookieMap();

  	if (commonHeader.isReady || !IS_IN_APP) {
  		callback && callback(commonHeader);
  		return;
  	}

  	WubaJSBridge$1.invoke('get_user_info', function (resp) {
  		var data = JSON.parse(resp);

  		// 放在此处存在PC上一直是未ready状态
  		// 调试时比较麻烦,且存在多次调用get_user_info的情况
  		// 看下在外层做下hack
  		commonHeader.isReady = true;

  		for (var key in data) {
  			appendHeader(key, data[key]);
  		}

  		callback && callback(commonHeader);
  	});
  }

  var ready = {
  	commonHeader: commonHeader,
  	JSBridgeReady: JSBridgeReady
  };

  var getVersion = getAppVersion;

var util = Object.freeze({
  	getVersion: getVersion,
  	getType: getType,
  	compareVersion: compareVersion,
  	createURI: createURI,
  	adapterProtocol: adapterProtocol
  });

  var common = {
  	ENV: ENV,
  	version: APP_VERSION,
  	appVersion: APP_VERSION,
  	sdkVersion: '1.2.5'
  };

  /**
   * 初始化CommonHeader数据
   */
  /**
   * commonHeader数据准备好之后，即获取用户数据时既则可以直接通过读取 headers对象上的数据获取
   * 	此处是通过action获取的，若native能保障cookie的数据一致，则可以通过cookie获取，省去异步的坑
   */
  var app$1 = extend(WubaJSBridge$1, {
    action: actions,
    extend: extendFuncs,
    ready: ready.JSBridgeReady,
    headers: ready.commonHeader,
    common: common,
    util: util
  });

  void 0 === window.WubaJSBridge && (window.WubaJSBridge = app$1);

  var asyncGenerator = function () {
    function AwaitValue(value) {
      this.value = value;
    }

    function AsyncGenerator(gen) {
      var front, back;

      function send(key, arg) {
        return new Promise(function (resolve, reject) {
          var request = {
            key: key,
            arg: arg,
            resolve: resolve,
            reject: reject,
            next: null
          };

          if (back) {
            back = back.next = request;
          } else {
            front = back = request;
            resume(key, arg);
          }
        });
      }

      function resume(key, arg) {
        try {
          var result = gen[key](arg);
          var value = result.value;

          if (value instanceof AwaitValue) {
            Promise.resolve(value.value).then(function (arg) {
              resume('next', arg);
            }, function (arg) {
              resume('throw', arg);
            });
          } else {
            settle(result.done ? 'return' : 'normal', result.value);
          }
        } catch (err) {
          settle('throw', err);
        }
      }

      function settle(type, value) {
        switch (type) {
          case 'return':
            front.resolve({
              value: value,
              done: true
            });
            break;

          case 'throw':
            front.reject(value);
            break;

          default:
            front.resolve({
              value: value,
              done: false
            });
            break;
        }

        front = front.next;

        if (front) {
          resume(front.key, front.arg);
        } else {
          back = null;
        }
      }

      this._invoke = send;

      if (typeof gen.return !== 'function') {
        this.return = undefined;
      }
    }

    if (typeof Symbol === 'function' && Symbol.asyncIterator) {
      AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
        return this;
      };
    }

    AsyncGenerator.prototype.next = function (arg) {
      return this._invoke('next', arg);
    };

    AsyncGenerator.prototype.throw = function (arg) {
      return this._invoke('throw', arg);
    };

    AsyncGenerator.prototype.return = function (arg) {
      return this._invoke('return', arg);
    };

    return {
      wrap: function (fn) {
        return function () {
          return new AsyncGenerator(fn.apply(this, arguments));
        };
      },
      await: function (value) {
        return new AwaitValue(value);
      }
    };
  }();

  var classCallCheck = function (instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError('Cannot call a class as a function');
    }
  };

  var createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ('value' in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  /**
   * @description 数据转换器
   */

  var Transfer = function () {
      function Transfer() {
          classCallCheck(this, Transfer);

          this._object = {};
          this._undefined_ = '__Undefined__';
          this._is = isClass;
          this._extend = extend;
          this._deepClone = deepClone;
      }

      createClass(Transfer, [{
          key: '_walk',
          value: function _walk(key, value) {
              var me = this;
              var keyAry = key.split('.');
              var len = keyAry.length;
              var object = {};
              object[keyAry[len - 1]] = me._deepClone(value);
              for (var i = len - 2; i >= 0; i--) {
                  var step = {};
                  step[keyAry[i]] = me._deepClone(object);
                  object = step;
              }
              return object;
          }
      }, {
          key: '_get',
          value: function _get(key) {
              var me = this;
              var keyAry = key.split('.');
              var result = this._object[keyAry[0]];
              for (var i = 1; i < keyAry.length; i++) {
                  result = result[keyAry[i]];
                  if (me._is(result) !== 'Object' && me._is(result) !== 'Array') {
                      if (i !== keyAry.length - 1) return undefined;
                      break;
                  }
              }
              return result;
          }
          /**
           * 初始化设置数据
           * @param object [Object]  -
           */

      }, {
          key: 'init',
          value: function init(object) {
              this._object = this._extend(true, {}, object);
              return this;
          }

          /**
           *
           */

      }, {
          key: 'set',
          value: function set(key, value) {
              var me = this;
              this._object = this._extend(true, this._object, me._walk(key, value));
              return this;
          }
      }, {
          key: 'add',
          value: function add(key, value) {
              this.set(key, value);
              return this;
          }
      }, {
          key: 'delete',
          value: function _delete(key) {
              var me = this;
              var keyAry = key.split('.');
              var len = keyAry.length;
              var object = this._object;

              for (var i = 0; i < len - 1; i++) {
                  object = object[keyAry[i]];
                  if (me._is(object) !== 'Object') {
                      if (i < len - 1) {
                          return me;
                      }
                      break;
                  }
              }

              try {
                  delete object[keyAry[len - 1]];
              } catch (e) {
                  object[keyAry[len - 1]] = undefined;
              }

              return this;
          }
      }, {
          key: 'replace',
          value: function replace(key, source, target) {
              if (this._get(key) == source) {
                  this.set(key, target);
              }
              return this;
          }
      }, {
          key: 'clone',
          value: function clone(target, source) {
              // console.log(target,this._get(source));
              this.set(target, this._get(source));
              return this;
          }
      }, {
          key: 'move',
          value: function move(source, target) {
              this.clone(target, source);
              this.delete(source);
              return this;
          }
      }, {
          key: 'extend',
          value: function extend(source) {
              this._extend(true, this._object, source);
              return this;
          }
      }, {
          key: 'filter',
          value: function filter(fn) {
              fn.call(this, this._object);
              return this;
          }
      }, {
          key: 'clear',
          value: function clear() {
              this._object = {};
              return this;
          }
      }, {
          key: 'toJSON',
          value: function toJSON() {
              return this._object;
          }
      }]);
      return Transfer;
  }();

  var ADAPTERS = {};

  /**
   * 通过actionName，返回一个定义的adapter
   * @param  {String} actionName
   * @return {function}
   */
  function getAdapter(actionName) {
      return ADAPTERS[actionName];
  }

  var Adapter = function () {
      function Adapter(actionName) {
          classCallCheck(this, Adapter);

          this.actionName = actionName;
          this.os = OS;
          this.appVersion = APP_VERSION;
      }

      /**
       * override
       */


      createClass(Adapter, [{
          key: 'method',
          value: function method(params, fn) {
              return {
                  actionName: this.actionName,
                  params: params,
                  callback: fn
              };
          }

          // 不能改写！！！

      }, {
          key: 'install',
          value: function install() {
              var force = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

              var me = this;
              var actionName = me.actionName;
              if (!force) {
                  for (var key in ADAPTERS) {
                      if (key == actionName) return false;
                  }
              }

              // function callMethod(params,fn){
              //     return me.method.call(me,params,fn) ;
              // }
              // ADAPTERS[actionName] = callMethod ;
              ADAPTERS[actionName] = me.method.bind(me);
              return true;
          }
      }]);
      return Adapter;
  }();

  

  Adapter.Transfer = Transfer;
  Adapter.utils = {
      compareVersion: compareVersion,
      isClass: isClass,
      optimizeCb: optimizeCb
  };

  // ---------------------------
  /*
  * 增加装饰函数，用于参数校验
  */
  // ---------------------------
  function before(fn, beforeFn) {
  	return function () {
  		if (beforeFn.apply(this, arguments) === false) {
  			return false;
  		}
  		return fn.apply(this, arguments);
  	};
  }

  function _send(message) {
  	var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  	var strategy = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '-_v';

  	var _message = '[' + strategy + '] ' + type + ' error : ' + message;
  	return _message;
  }

  // 验证类
  var strategies = {
  	isNonEmpty: function isNonEmpty(value) {
  		var errorMsg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '参数不能为空';

  		// console.log(value) ;
  		if (value.length <= 0) {
  			return _send(errorMsg, 'value', 'isNonEmpty');
  		}
  	},

  	isRequired: function isRequired(value) {
  		var errorMsg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '参数为必填项';

  		if (value === undefined) {
  			return _send(errorMsg, 'value', 'isRequired');
  		}
  	},

  	maxLength: function maxLength(value, length) {
  		var errorMsg = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '字符串长度限制最大 ' + length + '个字符';

  		if (value.length > length) {
  			return _send(errorMsg, 'value', 'maxLength');
  		}
  	},

  	minLength: function minLength(value, length) {
  		var errorMsg = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '字符串长度限制最小 ' + length + '个字符';

  		if (value.length < length) {
  			return _send(errorMsg, 'value', 'minLength');
  		}
  	},

  	hasKeys: function hasKeys() {
  		var object = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  		var keys = arguments[1];
  		var errorMsg = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '对象中必须包含的key值有:' + keys.split('|').join(',');

  		var _keys = keys.split('|');
  		var _dropKeys = [];

  		for (var i = 0, key; key = _keys[i++];) {
  			if (object[key] === undefined) {
  				_dropKeys.push(key);
  			}
  		}

  		if (_dropKeys.length > 0) {
  			return _send(errorMsg, 'value', 'hasKeys');
  		}
  	},

  	isContain: function isContain(value, options) {
  		var errorMsg = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '该值不在可选 ' + options + ' 范围内';

  		var _options = options.split('|');
  		var _isExsist = false;

  		for (var i = 0, option; option = _options[i++];) {
  			if (option === value) {
  				_isExsist = true;
  				break;
  			}
  		}

  		if (!_isExsist) {
  			return _send(errorMsg, 'value', 'isContain');
  		}
  	},

  	regExp: function regExp(value) {
  		var errorMsg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '该值与表达式不匹配';
  		var opts = arguments[2];

  		var reg = opts.regExp;
  		if (!reg.test(value)) {
  			return _send(errorMsg, 'value', 'regExp');
  		}
  	}

  };

  // 添加基本类型 strategies

  TYPE_DATA.forEach(function (type) {
  	var strategy = 'is' + type;
  	// var _type = type.toLowerCase() ;

  	strategies[strategy] = function (type, strategy) {
  		return function (value) {
  			var errorMsg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '参数数据类型错误';

  			if (getType(value) !== type) {
  				return _send(errorMsg, 'type', strategy);
  			}
  		};
  	}(type, strategy);
  });

  function Validator() {
  	this.cache = []; // 保存校验规则
  }

  // add ：添加校验规则并缓存在 this.cache 中
  //  field:需要校验的参数
  //  rules：校验规则 Array
  // 		每条规则中包含的参数形式为
  // 		strategy:"规则名" 在 strategies 中查找 必填
  // 						若校验规则中需要其他的参数,可以这样用:
  //						strategy:"strategyName:param1:param2:param3..."(最好为基本数据类型，其他数据类型可能会有问题)
  //		errorMsg:"校验不通过的提示信息"  必填(
  //						与strateties中添加的校验规则方法相关，若存在默认值也可以不填)
  //

  Validator.prototype.add = function (field, rules) {
  	var self = this;

  	for (var i = 0, rule; rule = rules[i++];) {
  		(function (rule) {
  			var strategyAry = rule.strategy.split(':');
  			var errorMsg = rule.errorMsg;
  			var extendOptions = {};
  			for (var key in rule) {
  				extendOptions[key] = rule[key];
  			}

  			self.cache.push(function () {
  				var strategy = strategyAry.shift();
  				strategyAry.unshift(field);
  				strategyAry.push(errorMsg);
  				strategyAry.push(extendOptions);
  				return strategies[strategy].apply(null, strategyAry);
  			});
  		})(rule);
  	}
  };

  // start:开始校验
  // 取出this.cache中的校验规则，真正的执行校验
  // 会执行所有的校验规则，并返回一个错误提示信息的集合(Array)
  // 无错误信息时返回空数组
  Validator.prototype.start = function () {
  	var errorMsgAry = [];
  	for (var i = 0, validatorFun; validatorFun = this.cache[i++];) {
  		var errorMsg = validatorFun();
  		if (errorMsg) {
  			// return errorMsg ;
  			errorMsgAry.push(errorMsg);

  			return errorMsgAry;
  		}
  	}

  	return errorMsgAry;
  };

  function sendMessages(errorMsgAry) {
  	var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'error';

  	if (errorMsgAry.length <= 0) {
  		return true;
  	}
  	for (var i = 0, info; info = errorMsgAry[i++];) {
  		console[type](info);
  	}

  	return false;
  }

  function bindAccount$1(type, callback) {
  	var validator = new Validator();

  	validator.add(type, [{
  		strategy: 'isRequired'
  	}, {
  		strategy: 'isString'
  	}, {
  		strategy: 'isContain:WEIXIN|QQ|PHONE'
  	}]);

  	var messages = validator.start();

  	return sendMessages(messages);
  }

  function cancelAlarm$1(n) {
  	var validator = new Validator();

  	validator.add(n, [{
  		strategy: 'isRequired',
  		errorMsg: 'n 为必填项'
  	}, {
  		strategy: 'isString'
  	}, {
  		strategy: 'isNonEmpty'
  	}]);

  	return sendMessages(validator.start());
  }

  function dialog$2(type) {
  	var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  	var callback = arguments[2];

  	var validator = new Validator();

  	validator.add(type, [{
  		strategy: 'isRequired'
  	}, {
  		strategy: 'isString'
  	}, {
  		strategy: 'isContain:single|double'
  	}]);

  	return sendMessages(validator.start());
  }

  function extendBtn$1(btnGroup, callback) {
  	var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'vertical';
  	var cmd = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'init';

  	var validator = new Validator();

  	validator.add(btnGroup, [{
  		strategy: 'isRequired',
  		errorMsg: 'btnGroup 为必填项'
  	}, {
  		strategy: 'isArray',
  		errorMsg: 'btnGroup 需要为数组'
  	}]);

  	if (btnGroup !== undefined && btnGroup.length > 0) {
  		for (var i = 0, item; item = btnGroup[i++];) {
  			console.log(item);
  			validator.add(item, [{
  				strategy: 'hasKeys:key|txt',
  				errorMsg: 'btnGroup的数组中对象必须包含 txt|key 键值对'
  			}]);
  		}
  	}

  	validator.add(type, [{
  		strategy: 'isContain:horizontal|vertical',
  		errorMsg: 'type字段标识按钮展示类型，支持的值为 horizontal/vertical'
  	}]);

  	validator.add(cmd, [{
  		strategy: 'isContain:init',
  		errorMsg: 'cmd目前仅支持的值为 init ,该字段为保留字段'
  	}]);

  	// validator.add(callback,[{
  	// 	strategy:"isRequired" ,
  	// 	errorMsg:"callback 为必填项"
  	// }]) ;


  	var messages = validator.start();
  	return sendMessages(messages);
  }

  function getUserInfo$1() {
  	var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  	var callback = arguments[1];

  	var validator = new Validator();

  	validator.add(key, [{
  		strategy: 'isString'
  	}]);

  	validator.add(callback, [{
  		strategy: 'isRequired',
  		errorMsg: 'callback为必填项'
  	}]);

  	return sendMessages(validator.start());
  }

  function goback$1() {
  	var isBacktoroot = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

  	var validator = new Validator();

  	validator.add(isBacktoroot, [{
  		strategy: 'isBoolean'
  	}]);

  	return sendMessages(validator.start());
  }

  function initSearch$1(cateId, listName, cate, placeholder) {
  	var validator = new Validator();

  	// cateId
  	validator.add(cateId, [{
  		strategy: 'isRequired',
  		errorMsg: 'cateId 为必填项'
  	}, {
  		strategy: 'isString',
  		errorMsg: 'cateId 数据类型错误'
  	}]);

  	// listName
  	validator.add(listName, [{
  		strategy: 'isRequired',
  		errorMsg: 'listName 为必填项'
  	}, {
  		strategy: 'isString',
  		errorMsg: 'listName String数据类型'
  	}, {
  		strategy: 'isNonEmpty',
  		errorMsg: 'listName 不能为空字符串'
  	}]);

  	// logTxt
  	validator.add(cate, [{
  		strategy: 'isRequired',
  		errorMsg: 'cate 为必填项'
  	}, {
  		strategy: 'isString',
  		errorMsg: 'cate String数据类型'
  	}, {
  		strategy: 'isNonEmpty',
  		errorMsg: 'cate 不能为空字符串'
  	}]);

  	var messages = validator.start();

  	return sendMessages(messages);
  }

  function installApp$1(url) {
  	var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'browser';

  	var validator = new Validator();

  	validator.add(url, [{
  		strategy: 'isRequired'
  	}, {
  		strategy: 'isString'
  	}]);

  	validator.add(type, [{
  		strategy: 'isContain:browser|native',
  		errorMsg: 'type 字段针对Android系统有用，可选值为 browser|native 。'
  	}]);

  	return sendMessages(validator.start());
  }

  function isBindAccount$1(type, callback) {
  	var validator = new Validator();

  	validator.add(type, [{
  		strategy: 'isRequired'
  	}, {
  		strategy: 'isString'
  	}, {
  		strategy: 'isContain:WEIXIN|QQ|PHONE'
  	}]);

  	validator.add(callback, [{
  		strategy: 'isRequired'
  	}]);

  	var messages = validator.start();

  	return sendMessages(messages);
  }

  function loadingbar$1(cmd) {
  	var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '1';

  	var validator = new Validator();

  	validator.add(cmd, [{
  		strategy: 'isRequired',
  		errorMsg: 'cmd 为必填项'
  	}, {
  		strategy: 'isString',
  		errorMsg: 'cmd 需要String类型数据'
  	}, {
  		strategy: 'isContain:show|hide',
  		errorMsg: 'cmd 可选值为：show|hide'
  	}]);

  	validator.add(type, [{
  		strategy: 'isString',
  		errorMsg: 'type 不能为空'
  	}, {
  		strategy: 'isContain:1|2',
  		errorMsg: 'type 可选值为：1(局部半透明)|2(全屏模式)'
  	}]);

  	var errorMsg = validator.start();

  	// validator = null ;

  	sendMessages(errorMsg);
  	if (!!errorMsg.length) {
  		return false;
  	}
  }

  function pagetrans$3(tradeline, content) {
  	var validator = new Validator();

  	validator.add(tradeline, [{
  		strategy: 'isRequired',
  		errorMsg: 'tradeline参数为必填项'
  	}, {
  		strategy: 'isString',
  		errorMsg: 'tradeline参数需要为字符串类型'
  	}]);

  	validator.add(content, [{
  		strategy: 'isRequired',
  		errorMsg: 'content参数为必填项'
  	}, {
  		strategy: 'isObject',
  		errorMsg: 'content需要为Object类型'
  	}]);

  	var messages = validator.start();

  	return sendMessages(messages);
  }

  function setAlarm$1(config) {
  	var validator = new Validator();

  	validator.add(config, [{
  		strategy: 'hasKeys:title|alert|time|n|content'
  	}]);

  	validator.add(config.time, [{
  		strategy: 'regExp',
  		regExp: /^\d{4}(-\d{2}){2}\s\d{2}(:\d{2}){2}$/i,
  		errorMsg: '日期须为形如  YYYY-MM-DD hh:mm:ss 格式'
  	}]);

  	var messages = validator.start();

  	return sendMessages(messages);
  }

  function setTitle$1(title) {
  	var validator = new Validator();
  	validator.add(title, [{
  		strategy: 'isRequired',
  		errorMsg: 'title为必填项'
  	}, {
  		strategy: 'isString',
  		errorMsg: 'title 需要为String类型'
  	}, {
  		strategy: 'isNonEmpty',
  		errorMsg: 'title不能为空'
  	}]);

  	var errorMsg = validator.start();

  	return sendMessages(errorMsg);
  }

  function share$2(config, callback) {
  	var validator = new Validator();

  	validator.add(config, [{
  		strategy: 'isRequired',
  		errorMsg: 'config 为必填项'
  	}, {
  		strategy: 'isArray',
  		errorMsg: 'config 为数组类型，数组中包含分享数据'
  	}, {
  		strategy: 'isNonEmpty',
  		errorMsg: 'config 不能为空数组 '
  	}]);

  	if (config !== undefined && config.length > 0) {
  		for (var i = 0, item; item = config[i++];) {

  			validator.add(item.shareto, [{
  				strategy: 'isRequired',
  				errorMsg: '分享数据中 shareto 为必填值'
  			}, {
  				strategy: 'isString',
  				errorMsg: '分享数据中 shareto 为字符串类型'
  			}, {
  				strategy: 'isContain:QQ|WEIXIN|SINA|FRIENDS',
  				errorMsg: '分享数据中 shareto 的可选值为：QQ|WEIXIN|SINA|FRIENDS  '
  			}]);

  			if (item.type !== undefined && item.type === 'imgshare') {
  				validator.add(item, [{
  					strategy: 'hasKeys:shareto|type|data_url',
  					errorMsg: '分享图片时，分享数据中必须包含：shareto|type|data_url'
  				}]);
  			} else {
  				validator.add(item, [{
  					strategy: 'hasKeys:shareto|url',
  					errorMsg: '分享链接时，分享数据中必须包含：shareto|url'
  				}]);
  			}
  		}
  	}

  	return sendMessages(validator.start());
  }

  function imgPreview$1(imgs) {
  	var current = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  	var txts = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

  	var validator = new Validator();

  	validator.add(imgs, [{
  		strategy: 'isRequired',
  		errorMsg: 'imgs 为必填项'
  	}, {
  		strategy: 'isArray',
  		errorMsg: 'imgs 为数组类型'
  	}, {
  		strategy: 'isNonEmpty',
  		errorMsg: 'imgs 不能为空数组'
  	}]);

  	validator.add(current, {
  		strategy: 'isNumber',
  		errorMsg: 'current 为Number数据类型'
  	});

  	return sendMessages(validator.start());
  }

  function toast$1(msg) {
  	var validator = new Validator();
  	validator.add(msg, [{
  		strategy: 'isRequired'
  	}, {
  		strategy: 'isString'
  	}, {
  		strategy: 'isNonEmpty'
  	}]);

  	return sendMessages(validator.start());
  }

  function toggleGesture$1(cmd) {
  	var validator = new Validator();
  	if (cmd !== undefined) {
  		validator.add(cmd, [{
  			strategy: 'isString'
  		}, {
  			strategy: 'isContain:on|off'
  		}]);
  	}

  	return sendMessages(validator.start());
  }

  function uploadImg(cateId, listName, callback, opts) {
  	var validator = new Validator();

  	// cateId
  	validator.add(cateId, [{
  		strategy: 'isRequired',
  		errorMsg: 'cateId 为必填项'
  	}, {
  		strategy: 'isString',
  		errorMsg: 'cateId 需为字符串类型'
  	}, {
  		strategy: 'isNonEmpty',
  		errorMsg: 'cateId 不能为空字符串'
  	}]);

  	// listName
  	validator.add(listName, [{
  		strategy: 'isRequired',
  		errorMsg: 'listName 为必填项'
  	}, {
  		strategy: 'isString',
  		errorMsg: 'listName 需为字符串类型'
  	}, {
  		strategy: 'isNonEmpty',
  		errorMsg: 'listName 不能为空字符串'
  	}]);

  	// callback
  	validator.add(callback, [{
  		strategy: 'isRequired',
  		errorMsg: 'callback 为必填项'
  	}]);

  	// sourceType
  	// validator.add(sourceType,[{
  	// 	strategy:'isContain:0|1|2'
  	// }]) ;


  	var messages = validator.start();

  	return sendMessages(messages);
  }

  function setWeblog$1(pageType, actionType, opts) {
  	var validator = new Validator();
  	validator.add(pageType, [{
  		strategy: 'isRequired',
  		errorMsg: 'pageType 为必填项'
  	}, {
  		strategy: 'isNonEmpty',
  		errorMsg: 'pageType 不能为空字符串'
  	}]);

  	validator.add(actionType, [{
  		strategy: 'isRequired',
  		errorMsg: 'actionType 为必填项'
  	}, {
  		strategy: 'isNonEmpty',
  		errorMsg: 'actionType 不能为空字符串'
  	}]);

  	if (opts !== undefined) {
  		validator.add(opts, [{
  			strategy: 'isObject',
  			errorMsg: 'opts 为Object对象数据，对象中可包含的字段为 params(参数) 及 cate'
  		}]);
  	}

  	if (opts !== undefined && opts.params !== undefined) {
  		validator.add(opts.params, [{
  			strategy: 'isArray',
  			errorMsg: 'opts params 为数组类型'
  		}, {
  			strategy: 'isNonEmpty',
  			errorMsg: 'opts params 不能为空数组'
  		}]);
  	}

  	if (opts !== undefined && opts.cate !== undefined) {
  		validator.add(opts.cate, [{
  			strategy: 'isString',
  			errorMsg: 'opts cate 为字符串类型'
  		}, {
  			strategy: 'isNonEmpty',
  			errorMsg: 'opts cate 不能为空字符串'
  		}]);
  	}

  	return sendMessages(validator.start());
  }

  var action = {
      bindAccount: bindAccount$1,
      cancelAlarm: cancelAlarm$1,
      dialog: dialog$2,
      extendBtn: extendBtn$1,
      getUserInfo: getUserInfo$1,
      goback: goback$1,
      initSearch: initSearch$1,
      installApp: installApp$1,
      isBindAccount: isBindAccount$1,
      loadingbar: loadingbar$1,
      pagetrans: pagetrans$3,
      setAlarm: setAlarm$1,
      setTitle: setTitle$1,
      share: share$2,
      imgPreview: imgPreview$1,
      toast: toast$1,
      toggleGesture: toggleGesture$1,
      uploadImg: uploadImg,
      setWeblog: setWeblog$1
  };

  // 模块数据
  var moduleData = extend(true, {}, {
      action: action
  });

  // 安装该module的方法
  function install(source, validators) {
      for (var key in validators) {
          var validator = validators[key];
          var valiType = getType(validator);
          var fn = source[key];
          var fnType = getType(fn);
          if (valiType === 'Object' && fnType === 'Object') {
              source[key] = install(fn, validator);
              continue;
          }
          if (valiType === 'Function' && fnType === 'Function') {
              source[key] = before(fn, validator);
          }
      }

      return source;
  }

  // 模块名字
  var moduleName = 'validator';

var validator = Object.freeze({
      moduleData: moduleData,
      install: install,
      moduleName: moduleName
  });

  /**
   * 支付
   * @param  {Object} param
   */
  // _mark = 1  的时候是来自 转转优品
  // _mark = 2  的时候来自 个人中心支付
  // _mark = 10 的时候来自 发布模块
  function get_pay$1(params) {
      params = extend(true, {}, params);
      var payLink = (location.protocol === 'https:' ? 'https:' : 'http:') + '//paycenter.58.com/pay?';
      var mark = params['_mark'];
      var balancePaid = params['balancePaid'];
      var payType = params['payType'] || 'balance|wechat';
      var filterTypes = payType.split('|').filter(function (element, index, array) {
          return element != 'wap' && element != 'alipay';
      }).join('|');
      // window._jumpinfo里面含有是否来自微信的字段
      // wlsour=weixin来判断来自微信
      var jumpInfo = window['_jumpinfo'] || {};
      var fromwechat = jumpInfo.wlsource === 'weixin';
      var payforh5 = false;
      var payforsdk = false;
      var forcepayforsdk = false;
      var pass = true;

      // 新增参数：accountType & balanceType
      // accountType:业务线
      // babanceType:支付流程 100：老流程 ；200：新流程
      /**
       * 针对新参数处理：
       * 如果accountType无值，则将balanceType值置为 100
       * balanceType的值默认为100，即老流程
       */
      var _params = params,
          _params$balanceType = _params.balanceType,
          balanceType = _params$balanceType === undefined ? '100' : _params$balanceType,
          accountType = _params.accountType;

      if (!accountType && accountType != 0) {
          balanceType = '100';
      }
      params['balanceType'] = balanceType;

      if (mark == 1 || mark == 2) {
          payforh5 = true;
      }

      //判断是否需要用H5支付 balancePaid表示支付的时候需要用该字段进行加密来生成sign值
      if (balancePaid != 0 && balancePaid != 1 || mark === 10) {
          payforsdk = true;
      }
      //判断是否统一使用支付SDK来支付
      if (compareVersion(APP_VERSION, '7.4.0')) {
          forcepayforsdk = true;
      }

      // 获取跳转的url
      function genUrl(params) {
          var url = payLink;
          var querys = [];
          params['platfrom'] = 'mobile';
          for (var key in params) {
              var value = params[key];
              if (value === null || value === undefined) {
                  continue;
              }
              querys.push(key + '=' + value);
          }
          querys.push('os=' + OS);
          url += querys.join('&');
          return url;
      }

      /**
      * 临时测试iappay支付方案，仅限productorid为19的iOS马甲包，正式环境必须重新设计
      */
      /**临时解决方案 start */
      // if(window.__pid && window.__pid != '19' && param['payType']){
      //   param['payType'] = param['payType'].split('|').filter(function(element, index, array) {
      //         return (element != 'iappay');
      //       }).join('|');
      // }
      // if(this.os == 'ios' && param['payType'].indexOf('iappay') != -1){
      //   param['IAPProductID'] = 'com.58tongchengzhaopin.iphone.wbpay1';
      //   param['IAPProductCount'] = parseInt(param['orderMoney']);
      // }
      /**临时解决方案 end */

      //转转优品用户体系独立，无法共用cookie，只能用当前页面跳转的H5支付方式
      // alert(param['_mark']);
      if (mark == 1) {
          var url = genUrl(params);
          location.href = url;
          return {
              pass: false,
              params: {}
          };
      }

      // 不能识别的支付方式
      if (forcepayforsdk && fromwechat && filterTypes === '' || (payforh5 || fromwechat) && payforsdk && filterTypes === '') {
          return {
              pass: true,
              params: {
                  action: 'toast',
                  msg: '抱歉，暂不支持当前支付方式，请使用网页版进行支付！'
              }
          };
      }

      if (forcepayforsdk) {
          if (fromwechat) {
              params['payType'] = filterTypes;
          }
          if (mark == 10) {
              delete params.balancePaid;
              delete params.sign;
          }
          return { params: params };
      }

      if (payforh5 || fromwechat) {
          if (payforsdk) {
              params['payType'] = filterTypes;
              return { params: params };
          }

          var _url = genUrl(param);
          if (fromwechat) {
              _url += '&fromwx=1';
          }

          // WBAPP.loadPage("link", url, "支付", false, false, true, "hide", false, false, true);

          return {
              pass: true,
              params: {
                  action: 'loadpage',
                  pagetype: 'link',
                  url: _url,
                  title: '支付'
              }
          };
      }

      // 原数据返回
      return { params: params };
  }

  function pagetrans$4(params) {
      var isVendor = getCookie('intermanufacturer') ? true : false;
      var isErrorVersion = compareVersion(APP_VERSION, '7.1.5') === 0;

      if (isVendor && isErrorVersion) {
          if (params.pagetype && params.pagetype === 'common') {
              params.pagetype = 'link';
          }

          if (params.content && params.content.pagetype && params.content.pagetype === 'common') {
              params.content.pagetype = 'link';
          }
      }

      return { params: params };
  }

  var loadpage = pagetrans$4;

  var publishRootDomain = 'pwebapp.58.com';

  function setcateid(params) {
      var $ = window.$ || function () {};
      $.index = function () {};
      $.index.dopost = function (localid, cityid, regionid, businessid, geotype, geoia) {
          var source = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : '';

          try {
              var json4fe = typeof ____json4fe !== 'undefined' ? ____json4fe : {};
              var catentry = json4fe.catentry || {};
              var str_location = cityid + ',' + regionid + ',' + businessid;
              var url = location.protocol + '//' + publishRootDomain + '/';

              /**
               * 以下代码没有作用
               * 故移除
               * @type {[type]}
               */
              // let catename ;
              // if (catentry instanceof Array && catentry.length > 0){
              //     catename = catentry[0].listname;
              // }else if (typeof catentry != 'undefined' && typeof catentry.listname != 'undefined'){
              //     catename = catentry.listname;
              // }else{
              //     catename = 'shangjie'; //商街虚拟类别
              // }

              if (catentry.listname == 'house') {
                  url += 'publish/house?';
              } else {
                  url += localid + '/5/s5?s5&';
              }

              url += 'localid=' + localid + '&location=' + str_location + '&geotype=' + geotype + '&geoia=' + geoia + '&formatsource=' + source;

              WubaJSBridge$3.invoke('loadpage', {
                  pagetype: 'publish',
                  url: url,
                  title: catentry.name
              });
          } catch (ex) {
              console.log(ex);
              WubaJSBridge$3.debug && WubaJSBridge$3.invoke('toast', {
                  msg: ex.name + ': ' + ex.message
              });
          }
      };
      void 0 === window.$ && (window.$ = $);
      return { params: params };
  }

  /**
   * SDK升级兼容
   *  按旧SDK传入的参数，或按新SDK传入的参数，
   *  都能在各个版本正常使用
   */
  function get_recharge(params) {
      var rechargeType = params.rechargeType;
      var balanceType = params.balanceType;
      var accountType = params.accountType;

      // 按照新支付SDK传过来的参数，在就支付SDK亦需要兼容
      if (balanceType == '100' && accountType) {
          rechargeType = accountType;
      }

      if (balanceType == '200') {
          if (rechargeType != '0' || rechargeType != '1' || rechargeType != '2') {
              rechargeType = accountType;
              if (rechargeType - 0 > 2) {
                  rechargeType = '2';
              }
          }
      }

      // 兼容旧的支付SDK
      if (!balanceType) {
          balanceType = '100';
          accountType = rechargeType;
      }

      params = extend(params, {
          rechargeType: rechargeType,
          accountType: accountType,
          balanceType: balanceType
      });
      return { params: params };
  }

  function get_user_info$1(params) {
      var cb = optimizeCb(params.callback);
      var key = params.key;
      var wrapRespTypeCb = function wrapRespTypeCb(resp) {
          /* istanbul ignore if */
          if (getType(resp) === 'Object') {
              resp = JSON.stringify(resp);
          }
          cb && cb(resp);
      };

      var wrapCb = wrapRespTypeCb;
      /* istanbul ignore if */
      if (OS === 'android' && cb) {
          wrapCb = function patchWrapCb(response) {
              var resp = response;
              /**
              * 返回的数据大概为这样的情况：
              *   params={"xxkey":"xxValue"}
              *
              * 正常应为:
              *   params=`${encodeURIComponent(JSON.stringify({xxKey:xxValue}))}` ;
              */
              if (key === 'jumpinfo' && response && response.indexOf('params={') >= 0) {
                  try {
                      var pat = /params=(.*)/;
                      resp = 'params=' + encodeURIComponent(response.match(pat)[1]);
                  } catch (e) {
                      console.log(e);
                  }
              } else if (!key && response && response.indexOf('"jumpinfo":') >= 0) {
                  /**
                   * 数据异常基本同key=jumpinfo时
                   * 处理逻辑为：
                   *  先从字符串中取出encode后，
                   *  再插入字符串中
                   */
                  var isParseError = false;
                  try {
                      JSON.parse(response);
                  } catch (e) {
                      isParseError = true;
                  }

                  if (isParseError) {
                      try {
                          var _pat = /\"jumpinfo\":\"params=(\{.*\})\"/;
                          var mValue = response.match(_pat);
                          var rValue = encodeURIComponent(mValue[1]);
                          resp = response.replace(_pat, '"jumpinfo":"params=' + rValue + '"');
                          // 再尝试一次，以保证正确
                          JSON.parse(resp);
                      } catch (e) {
                          console.log(e);
                      }
                  }
              } else {
                  resp = response;
              }

              wrapRespTypeCb(resp);
          };
      }

      params.callback = wrapCb;
      return { params: params };
  }



  var patches$1 = Object.freeze({
  	get_pay: get_pay$1,
  	pagetrans: pagetrans$4,
  	loadpage: loadpage,
  	setcateid: setcateid,
  	get_recharge: get_recharge,
  	get_user_info: get_user_info$1
  });

  var doc$1 = document;
  var win$1 = window;
  var _CUSTOM_PROTOCOL_SCHEME$1 = 'nativechannel://';
  var _CALLBACK_NAMESPACE$1 = '__WubaJSBridge_Callback_OF__';
  var _FILTER_CB_MAP = {
  	'shareCallback': !0
  };
  var _PAGE_LINK = win$1.location.href;
  var _NOOP$1 = function _NOOP() {};
  var _sendMessageQueue$1 = [];
  var _iOSMessageFrameQueue$1 = [];
  var _os$1 = OS;
  var _appVersion$1 = APP_VERSION;
  var _isInApp$1 = IS_IN_APP;
  var _callback_unique_id$1 = 0;
  var _bridgeCore = _NOOP$1;
  var WubaJSBridge$2 = {
  	debug: false,
  	domReady: false
  };

  // 初始化jsbridge
  function _initJSBridge$1() {
  	var onReadyBridge = function onReadyBridge() {
  		document.removeEventListener('DOMContentLoaded', onReadyBridge);
  		if (WubaJSBridge$2.domReady) {
  			return;
  		}
  		WubaJSBridge$2.domReady = true;
  		_fetchQueue$1();
  	};

  	// 初始化bridge方法
  	_bridgeCore = function () {
  		if (!_isInApp$1) {
  			return _NOOP$1;
  		}

  		if (_os$1 === 'android' && win$1.stub && win$1.stub.jsCallMethod) {
  			return _bridgeAndroid$1;
  		}

  		if (_os$1 === 'ios') {
  			if (compareVersion(_appVersion$1, '6.2.5') <= 0) {
  				return _bridgeBelowIos;
  			}
  			return _bridgeIos$1;
  		}
  		return _NOOP$1;
  	}();

  	if (/complete|loaded|interactive/.test(doc$1.readyState) && doc$1.body) {
  		onReadyBridge();
  		return;
  	}

  	doc$1.addEventListener('DOMContentLoaded', onReadyBridge, false);
  }

  // 完整参数:actionName, params , callback , errorCallback
  //  params , callback ,errorCallback 参数可选且 params参数可缺省
  //  正确的参数形式：
  //      1. (actionName)
  //      2. (actionName,params)
  //      3. (actionName,params,callback)
  //      4. (actionName,params,callback , errorCallback)
  //      5. (actionName,callback)
  //  其他参数形式均不正确
  //
  //  参数类型:
  //      actionName:{String}
  //      params:{Object}
  //      callback:{Function || String}
  //      errorCallback:{String}
  //
  //  备注：
  //      callback 为String类型时，必须为全局函数的函数名，否则不能正确的调用方法
  function _call$1(actionName) {
  	var params = (arguments.length <= 1 ? undefined : arguments[1]) || {};
  	var cb = arguments.length <= 2 ? undefined : arguments[2];
  	var paramsType = getType(params);

  	if (!actionName) {
  		return;
  	}

  	// 处理传入参数问题
  	// 此处处理使得 参数形式 5 为正确参数形式
  	if ((arguments.length <= 1 ? 0 : arguments.length - 1) === 1 && (paramsType === 'Function' || paramsType === 'String')) {
  		cb = params;
  		params = {};
  	}
  	params = extend(true, {}, params);
  	params['action'] = actionName;
  	params['callback'] = cb;
  	_nativeBridge(params);
  	return params;
  }

  // for _nativeBridge
  function _nativeBridge(params) {
  	if (getType(params) !== 'Object') {
  		win$1.WubaJSBridge && win$1.WubaJSBridge.debug && console.error('Type Error: need object');
  		return;
  	}
  	var actionName = params.action;
  	var patcher = patches$1[actionName];
  	var patch = null;
  	var cb = null;
  	if (getType(patcher) === 'Function') {
  		patch = extend({
  			pass: true
  		}, patcher.call(patcher, params));
  		if (!patch.pass) {
  			return;
  		}
  		params = patch.params || params;
  	}
  	cb = params.callback;
  	if (cb) {
  		params['callback'] = _callback$1(cb);
  	}
  	params['action_handler'] = _PAGE_LINK;
  	_sendMessage$1(params);
  	// 该方案会产生大量数据
  	// 暂不处理
  	// 后续寻找到更合理的方案再处理
  	// _track(params.action, params);
  }

  // 发送消息到队列中
  function _sendMessage$1(message) {
  	_sendMessageQueue$1.push(message);
  	_fetchQueue$1();
  }

  // 请求(处理)消息队列
  function _fetchQueue$1() {
  	if (!WubaJSBridge$2.domReady || _sendMessageQueue$1.length <= 0) {
  		return;
  	}
  	var message = _sendMessageQueue$1.shift();
  	_bridgeCore(message);
  	_fetchQueue$1();
  	win$1.WubaJSBridge && win$1.WubaJSBridge.debug && console.info('->Native: %s', JSON.stringify(message));
  }

  /**
   * 用于制造回调函数
   * @param  {Function} fn   - 真正的回调函数
   * @param  {String}   name - 回调函数挂载安装成 CALLBACK_NAMESPACE[name]的形式，可选参数
   * @param  {mixin}   context - 回调的执行上下文
   * @param  {Boolean} execOnce -
   * @return {String}        - 字符串，用于传给native的回调函数名。如：WubaJSBridgeNativeInvokeCallback.JS_CALLBACK_1 ;
   */
  function _callback$1(cb) {
  	var name = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'JS_CALLBACK_' + ++_callback_unique_id$1;

  	// 先判断命名空间是否健在 ， 如有损坏则修复
  	if (getType(win$1[_CALLBACK_NAMESPACE$1]) !== 'Function') {
  		win$1[_CALLBACK_NAMESPACE$1] = _NOOP$1;
  	}
  	var cbType = getType(cb);

  	//  有些回调是Native写死的，不能覆盖
  	//  覆盖后会造成无法执行回调的问题
  	//  目前所知的是分享的回调在某些版本的问题
  	//  所以这部分回调需要单独filter
  	if (cbType === 'String' && _FILTER_CB_MAP[cb]) {
  		return cb;
  	}

  	if (cbType !== 'String' && cbType !== 'Function') {
  		return;
  	}
  	win$1[_CALLBACK_NAMESPACE$1][name] = _wrapCb$1(cb);
  	return _CALLBACK_NAMESPACE$1 + '.' + name;
  }

  // 处理回调函数参数类型不规范的错误
  function _wrapCb$1(cb) {
  	return function wrappedCb() {
  		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
  			args[_key] = arguments[_key];
  		}

  		var optimizeArgs = args.map(function (arg) {
  			if (getType(arg) === 'Number') {
  				return arg.toString();
  			}
  			// json
  			if (getType(arg) === 'Object') {
  				arg = JSON.stringify(arg);
  			}
  			// json字符串
  			if (getType(arg) === 'String' && arg.match(/^{.*}$/)) {
  				// 如果json字符串中有state或status参数
  				// 则该字段值强制转化为string类型
  				['state', 'status'].forEach(function (key) {
  					var reg = new RegExp('\"' + key + '\"\\s*:\s*(\\d+)', 'g');
  					arg = arg.replace(reg, '"' + key + '":"$1"');
  				});
  			}
  			return arg;
  		});
  		cb = optimizeCb(cb);
  		return cb.apply(cb, optimizeArgs);
  	};
  }

  // 是否复用？
  // 复用可能有问题
  // 同时请求的问题
  function _getIframe$1() {
  	if (_iOSMessageFrameQueue$1.length > 0) {
  		return _iOSMessageFrameQueue$1.shift();
  	}
  	var ifr = void 0;
  	ifr = doc$1.createElement('iframe');
  	ifr.setAttribute('style', 'display:none;');
  	ifr.setAttribute('height', '0px');
  	ifr.setAttribute('width', '0px');
  	ifr.setAttribute('frameborder', '0');
  	doc$1.body.appendChild(ifr);
  	return ifr;
  }

  //
  function _formatObj2Str(obj, join, sep) {
  	var ary = [];
  	for (var key in obj) {
  		ary.push(key + join + obj[key]);
  	}
  	return ary.join(sep);
  }

  // 简单封装的ajax
  function _ajax(settings) {
  	var params = _formatObj2Str(settings['data'], '=', '&');
  	if (settings['type'] == 'JSONP') {
  		var jsonpId = 'app58_jsonpcallback' + ++_callback_unique_id$1;
  		var script = document.createElement('script');
  		win$1[jsonpId] = settings['success'];
  		script.chareset = 'utf-8';
  		script.src = settings['url'] + '?' + params + '&callback=' + jsonpId;
  		script.id = jsonpId;
  		doc$1.body.appendChild(script);
  	} else {
  		var xhr = new XMLHttpRequest();
  		xhr.open(settings['type'], settings['url'], true);
  		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  		xhr.onreadystatechange = function () {
  			if (xhr.readyState == 4 && xhr.status == 200) {
  				settings['success'] && settings['success'].apply(null, [xhr.responseText]);
  			} else {
  				settings['error'] && settings['error'].apply(null, [xhr, xhr.responseText]);
  			}
  		};
  		xhr.send(params);
  	}
  }

  // 低版本iOS交互
  function _bridgeBelowIos(params) {
  	_ajax({
  		url: 'http://127.0.0.1/nativechannel/',
  		type: 'POST',
  		data: {
  			'paras': params
  		}
  	});
  }

  // ios交互
  function _bridgeIos$1(params) {
  	var ifr = _getIframe$1();
  	var message = _CUSTOM_PROTOCOL_SCHEME$1 + '?paras=' + encodeURIComponent(JSON.stringify(params));
  	ifr.src = message;
  	// recycle
  	setTimeout(function () {
  		_iOSMessageFrameQueue$1.push(ifr);
  	}, 20);
  }

  // android交互
  function _bridgeAndroid$1(params) {
  	win$1.stub.jsCallMethod(JSON.stringify(params));
  }

  function _packingParam(values, keys) {
  	var o = {};
  	for (var i = 0; i < values.length; i++) {
  		o[keys[i]] = values[i];
  	}
  	return o;
  }

  _initJSBridge$1();

  WubaJSBridge$2 = extend(WubaJSBridge$2, {
  	invoke: _call$1,
  	call: _call$1,
  	callback: _callback$1,
  	ajax: _ajax,
  	os: _os$1,
  	appVersion: _appVersion$1,
  	_nativeBridge: _nativeBridge,
  	_nativeBriadge: _nativeBridge,
  	_packingParam: _packingParam,
  	_fetchQueue: _fetchQueue$1
  });

  var WubaJSBridge$3 = WubaJSBridge$2;

  var setTitle$2 = new Adapter('set_title');

  setTitle$2.method = function setTitle(params) {
      return {
          actionName: 'changetitle',
          params: params,
          callback: undefined
      };
  };

  var isLogin$1 = new Adapter('is_login');
  isLogin$1.method = function isLogin(params, fn) {
      var optimizeCb = Adapter.utils.optimizeCb;
      return {
          actionName: 'islogin',
          params: params,
          callback: function isLoginCallback(data) {
              data = JSON.parse(data);
              var state = data.state == true ? '0' : '1';
              optimizeCb(fn, window)(state);
          }
      };
  };

  var setAlarm$2 = new Adapter('set_alarm');

  setAlarm$2.method = function setAlarm(params) {
      return {
          actionName: 'setalarm',
          params: params
      };
  };

  var share$3 = new Adapter('share');

  share$3.method = function (params, fn) {
  	var transfer = new Adapter.Transfer();
  	var optimizeCb = Adapter.utils.optimizeCb;

  	transfer.init(params).clone('data', 'config.0').move('data.img_url', 'data.picurl').move('data.data_url', 'data.dataURL').move('data.type', 'type').replace('type', 'imgshare', 'imageShare').delete('data.type').delete('data.shareto').filter(function (data) {
  		var shareTo = [];
  		var config = data.config;
  		config.forEach(function (item) {
  			shareTo.push(item.shareto);
  		});
  		data.shareto = shareTo.join(',');
  	}).clone('extshareto', 'shareto').delete('config');

  	window['shareCallback'] = function callback(state, source) {
  		if (getType(state) === undefined) {
  			return;
  		}
  		state = state.toString();
  		if (state === '0') {
  			state = '1';
  		} else if (state === '1') {
  			state = '0';
  		}
  		fn && optimizeCb(fn, window)(state, source);
  	};

  	return {
  		actionName: 'info_share',
  		params: transfer.toJSON(),
  		callback: 'shareCallback'
  	};
  };

  var deviceEvent$1 = new Adapter('device_event');

  deviceEvent$1.method = function (params, fn) {
      return {
          actionName: 'deviceevent',
          params: params,
          callback: fn
      };
  };

  var extendBtn$2 = new Adapter('extend_btn');

  extendBtn$2.method = function (params, fn) {
      var transfer = new Adapter.Transfer();
      var optimizeCb = Adapter.utils.optimizeCb;
      var key = params.config[0].key;

      transfer.init(params).clone('text', 'config.0.txt').set('type', 'top_right').set('enable', 'true').delete('config').delete('cmd');

      return {
          actionName: 'extend_btn',
          params: transfer.toJSON(),
          callback: function callback() {
              fn && optimizeCb(fn, window)(key, '0');
          }
      };
  };

  var login$1 = new Adapter('login');

  login$1.method = function login(params, fn) {
      var backwords = params.backwords;
      var utils = Adapter.utils;
      var pagetype = utils.compareVersion(this.appVersion, '7.2.0') >= 0 ? 'common' : 'link';
      if (utils.isClass(backwords) !== 'Object') {
          backwords = {
              'isReload': true,
              'url': location.href,
              'title': document.title,
              'pagetype': pagetype,
              'isFinish': true
          };
      }
      return {
          actionName: 'login',
          params: backwords,
          callback: null
      };
  };

  var setWebLog = new Adapter('weblog');

  setWebLog.method = function setWebLog(params, fn) {
      var transfer = new Adapter.Transfer();
      transfer.init(params).set('trackinfo', {}).move('page_type', 'trackinfo.pagetype').move('action_type', 'trackinfo.actiontype').move('cate', 'trackinfo.cate').move('params', 'trackinfo.params').set('forcesend', 'false');

      return {
          actionName: 'weblog',
          params: transfer.toJSON()
      };
  };

  var pagetrans$5 = new Adapter('pagetrans');

  pagetrans$5.method = function (params) {
      var me = this;
      var utils = Adapter.utils;
      var transfer = new Adapter.Transfer();
      transfer.init(params).filter(function (data) {
          /* istanbul ignore if */
          if (utils.compareVersion(me.appVersion, '7.2.0') < 0) {
              this.replace('content.pagetype', 'common', 'link');
          }
      });

      return {
          actionName: 'pagetrans',
          params: transfer.toJSON()
      };
  };

  var cancelAlarm$2 = new Adapter('cancel_alarm');

  cancelAlarm$2.method = function cancelAlarm(params) {
      return {
          actionName: 'cancelalarm',
          params: params
      };
  };

  var dialog$3 = new Adapter('dialog');

  dialog$3.method = function (params) {
      var transfer = new Adapter.Transfer();

      transfer.init(params).move('first_txt', 'btn1_txt').move('second_txt', 'btn2_txt');

      return {
          params: transfer.toJSON()
      };
  };

  var bindAccount$2 = new Adapter('bind_account');

  bindAccount$2.method = function (params) {
      params.autoBack = 'true';
      return {
          actionName: 'third_bind',
          params: params
      };
  };

  var adapters = {
      setTitle: setTitle$2,
      isLogin: isLogin$1,
      setAlarm: setAlarm$2,
      share: share$3,
      deviceEvent: deviceEvent$1,
      extendBtn: extendBtn$2,
      login: login$1,
      setWebLog: setWebLog,
      pagetrans: pagetrans$5,
      cancelAlarm: cancelAlarm$2,
      dialog: dialog$3,
      bindAccount: bindAccount$2
  };

  var hookArgsQueue = [];

  // 注册adapter
  function registerAdapter() {
      for (var key in adapters) {
          adapters[key].install();
      }
  }

  // 检测新老载体
  /* 如果版本低于7.2.0 及 android 且window对象有stub.jsCallMethod
  *  其他情况暂时无法确认,通过能力检测的方式确认
  */
  function checkWebview(settings, then) {
      var cb = function cb(webview) {
          then && then.call(then, webview);
      };
      settings = extend({
          OFInvoke: WubaJSBridge$3.invoke,
          NFInvoke: function NFInvoke() {},
          isInApp: IS_IN_APP,
          os: OS,
          appVersion: APP_VERSION
      }, settings);

      var NFInvoke = settings.NFInvoke;
      var OFInvoke = settings.OFInvoke;
      var isInApp = settings.isInApp;
      var appVersion = settings.appVersion;
      var os = settings.os;

      if (!isInApp) {
          cb('new');
          return;
      }

      if (compareVersion(appVersion, '7.2.0') < 0 || os == 'android' && window.stub && window.stub.jsCallMethod) {
          cb('old');
      } else {
          // 能力检测
          NFInvoke('is_login', function () {
              cb('new');
          });

          OFInvoke('islogin', function () {
              cb('old');
          });
      }
  }

  /**
   * 前置hook方法，即还没有注册真正的hook方法之前
   * @param  {mix} args
   * @return {undefined}
   */
  function preHook() {
      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
      }

      hookArgsQueue.push(args);
  }

  // 真正的hook方法
  function hook(actionName) {
      var params = (arguments.length <= 1 ? undefined : arguments[1]) || {};
      var callback = arguments.length <= 2 ? undefined : arguments[2];
      var paramsType = getType(params);

      // 处理传入参数问题
      // 此处处理使得 参数形式 5 为正确参数形式
      if ((arguments.length <= 1 ? 0 : arguments.length - 1) === 1 && paramsType !== 'Object' && (paramsType === 'Function' || paramsType === 'String')) {
          callback = params;
          params = {};
      }
      params = extend(true, {}, params);
      var defaultAdapter = function defaultAdapter() {
          return { pass: true, actionName: actionName, params: params, callback: callback };
      };
      var adapter = getAdapter(actionName) || defaultAdapter;
      var hookData = extend(true, {
          pass: true,
          actionName: actionName,
          callback: callback
      }, adapter(params, callback));

      if (!hookData.pass) {
          return;
      }
      return WubaJSBridge$3.invoke(hookData.actionName, hookData.params, hookData.callback);
  }

  // 模块名
  var moduleName$1 = 'adapter';

  // 模块数据
  var moduleData$1 = adapters;

  // 模块安装方法
  function install$1(app) {
      var invoke = app.invoke;
      app.invoke = app.call = preHook;
      checkWebview({
          NFInvoke: invoke
      }, function (webview) {
          if (webview == 'old') {
              registerAdapter();
              invoke = hook;
          }

          app.invoke = app.call = invoke;
          // 把未完成的调用自动完成
          hookArgsQueue.forEach(function (args) {
              app.invoke.apply(null, args);
          });

          hookArgsQueue = [];
      });

      return app;
  }

var adapter = Object.freeze({
      checkWebview: checkWebview,
      moduleName: moduleName$1,
      moduleData: moduleData$1,
      install: install$1
  });

  app$1.action.Adapter = Adapter;
  var modulesMap = {};
  modulesMap.validator = validator ;
  modulesMap.adapter = adapter;

  function genSDK() {
      var modules = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

      var app = app$1;
      for (var i = 0, moduleName; moduleName = modules[i++];) {
          var module = modulesMap[moduleName];
          if (getType(module) === 'Object' && getType(module.install) === 'Function') {
              app = module.install(app, module.moduleData);
          }
      }

      // 回收数据
      modulesMap = null;
      app.ready();
      return app;
  }

  var app = genSDK(['adapter', 'validator']);
  app.ready();

  return app;

}));