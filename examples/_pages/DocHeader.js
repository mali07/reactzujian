'use strict';

var React = require('react');
var AMUIReact = require('amazeui-react');
var Container = AMUIReact.Container;
var Button = AMUIReact.Button;
var Icon = AMUIReact.Icon;
var Nav = AMUIReact.Nav;
var NavItem = AMUIReact.NavItem;
var Badge = AMUIReact.Badge;
var LinkItem = require('./DocLinkItem');
var links = [
  {
    title: '开始使用',
    name: 'getting-started'
  },
  {
    title: '开发者',
    name: 'Devolopment'
  },
  {
    title: '组件',
    name: 'components'
  }
];

var DocHeader = React.createClass({
  getInitialState: function() {
    return {
      menuActive: false
    };
  },

  handleClick: function(e) {
    e && e.preventDefault();
    this.setState({
      menuActive: !this.state.menuActive
    });
  },

  handleDropdownClick: function() {
    if (matchMedia && matchMedia('(max-width: 640px)').matches) {
      this.handleClick();
    }
  },

  render: function() {
    var linkElements = links.map((link) => {
      return (
        <NavItem
          component={LinkItem}
          key={link.name}
          to={link.name}
          onClick={this.handleDropdownClick}
        >
          {link.title}
        </NavItem>
      );
    });
    var menuClass = this.state.menuActive ? ' am-in' : '';

    return (
      <header className="am-topbar am-topbar-inverse amr-header">
        <Container>
          <h1 className="am-topbar-brand">
            cheui-React
          </h1>
          <Button
            onClick={this.handleClick}
            className="am-show-sm-only am-topbar-btn am-topbar-toggle"
            amSize="sm" amStyle="secondary">
            <span className="am-sr-only">导航切换</span>
            <Icon icon="bars" />
          </Button>

          <div
            className={'am-collapse am-topbar-collapse' + menuClass} >
            <Nav pills topbar>
              {linkElements}
            </Nav>
          </div>
        </Container>
      </header>
    );
  }
});

module.exports = DocHeader;
