'use strict';

var React = require('react');
var AMUIReact = require('amazeui-react');
var Container = AMUIReact.Container;
var Banner = require('./DocBanner');
var Main = require('./DocMain');
var Markdown = require('../utils').Markdown;

var GettingStarted = React.createClass({
  render: function() {
    return (
      <Main>
        <Banner title="开发者指南">
          开发之前了解一下cheui-React开发都需要做哪些工作
        </Banner>
        <Container className="amr-content">
          <Markdown doc>{require('./devolopment.md')}</Markdown>
        </Container>
      </Main>
    );
  }
});

module.exports = GettingStarted;
