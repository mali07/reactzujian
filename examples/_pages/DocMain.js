'use strict';

var React = require('react');
var DocMain = React.createClass({
  render: function() {
    return (
      <main className="amr-main">
        {this.props.children}
      </main>
    );
  }

});

module.exports = DocMain;
