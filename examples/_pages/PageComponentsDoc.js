'use strict';
var React = require('react');
/**配置测试组件区域-新增组件后需要在这儿引入API*/
var Docs = {
  close: require('./../close/index'),
  icon:require('./../icon/index'),
  toast:require('./../toast/index'),
  pickerSelector:require('./../pickerSelector/index')
};
/**配置测试组件区域**/
var EmptyComponent = React.createClass({
  render: function() {
    return false;
  }
});
var PageComponentsDoc = React.createClass({
  render: function() {
    var component = this.props.params.component;
    var Doc = Docs[component] || EmptyComponent;

    return <Doc />;
  }
});

module.exports = PageComponentsDoc;
