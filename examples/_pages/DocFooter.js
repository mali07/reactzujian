'use strict';

var React = require('react');
var amazeui = require('amazeui-react');
var Icon = amazeui.Icon;
var Grid = amazeui.Grid;
var Col = amazeui.Col;
var Modal = amazeui.Modal;
var ModalTrigger = amazeui.ModalTrigger;
var year = new Date().getFullYear();
var socails = [
  // {
  //   name: 'qq',
  //   href: 'http://amazeui.org/about/contact'
  // },
  // {
  //   name: 'weibo',
  //   href: 'http://weibo.com/amazeui'
  // },
  {
    name: 'comments'
  },
  {
    name: 'github',
    href: 'http://gitlab.58corp.com/fe_ershouche/cheui-react'
  }
];

var modal = (<Modal
  className="amr-wechat-modal">
  <img src="http://s.amazeui.org/assets/2.x/i/am/qr-ui.jpg" alt=""/>
  加入美事上关注我们！
</Modal>);

var DocFooter = React.createClass({
  render: function() {
    return (
      <footer className="amr-footer">
        <Grid fixed>
          <Col md={12} mdPush={12}>
            <ul className="amr-footer-socials">
              {socails.map(function(s, i) {
                return (
                  <li key={i}>
                    {s.name === 'comments' ? (
                      <ModalTrigger
                        modalWidth="300"
                        modal={modal}>
                        <Icon
                          button
                          icon={s.name} />
                      </ModalTrigger>
                    ) : (
                    <Icon
                      button
                      href={s.href || null}
                      target="_blank"
                      icon={s.name} />)
                    }
                  </li>
                );
              })}
            </ul>
          </Col>
        </Grid>
      </footer>
    );
  }
});

module.exports = DocFooter;
