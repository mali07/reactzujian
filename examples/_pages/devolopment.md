##  如何开发一个cheui-react组件
> 基于React+Webpack+ES6,引入eslint，eslint-react统一编码风格，提前代码前请注意校验信息，不通过不允许提交代码。

### 创建新组件
```
yo react-webpack:component toast/toast //会在组件下新增一个toast文件夹以及ToastComponent.js，同时在styles文件夹下会创建一个toast文件夹和Toast.css 建议手动修改成.scss的文件
```

### 组件样例代码参考
```
'use strict'; 
import '/styles/toast/toastComponent.scss';
import {Component} from 'react';
import PropTypes from 'prop-types';
import ClassName from 'classnames';
import NativeBridge from 'component/nativeBridge';//58APPhiybird协议模块  如果是混合组件需要引入
class ToastComponent extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className = "cr-toast">
               <!-- dom -->
            </div>
        )
    }
    componentDidMount(){
    }
    /**
     * todo 添加注释模板
     *
     * @param {number} x The number to raise.
     * @param {number} n The power, must be a natural number.
     * @return {number} x raised to the n-th power.
     */
    toDo(x,n){
        return x;
    }
}

ToastComponent.propTypes = {
    msg: PropTypes.string.isRequired 
}

ToastComponent.defaultProps = {
    msg : ""
}
module.exports = ToastComponent;

```

### 如何测试你的代码
> 在编写代码的过程中，要求记录下组件的属性和对外的api；所以开发过程中需要执行npm run start 启动项目。
- **新增组件入口** ：在“组件”tab页签下，按照分类配置组件数据，操作文件：examples/_pages/data.js

```
module.exports = [
  {
    category: 'Common',
    title: '常用组件',
    components: [
      {
        id: 'close',
        name: 'Close',
        title: '关闭按钮'
      },{
        id: 'icon',
        name: 'Icon',
        title: '图标'
      }
    ]
  },
  {
    category: 'mobile',
    title: '移动端组件',
    components: [
      {
        id: 'toast', //对应测试样例文件夹名称
        name: 'Toast', //通常首字母大写
        title: 'toast提示'
      }
]

```

- **新增测试文件**：进入examples文件夹下，用组件名称新增一个文件夹（最好和component下的文件夹保持一致）
> 文件下，新增一个index.js文件,一个intro.md文件（组件属性介绍）

```
//index.js
'use strict';
var React = require('react');
var ReactBin = require('../ReactBin');
var utils = require('../utils');
var Markdown = utils.Markdown;
var Doc = utils.Doc;
/**样例配置区域 */
var examples = {
  basic: require('fs').readFileSync(__dirname + '/01-basic.js', 'utf-8') //测试样例
};
/**样例配置区域 */
var IconDoc = React.createClass({
  render: function() {
    return (
      <Doc>
        <h1>Toast</h1>
        <hr />
        <h2>组件介绍</h2>
        <Markdown>{require('./01-intro.md')}</Markdown>
        <h2>组件演示</h2>
        <h3>loadin效果</h3>
        <ReactBin code={examples.basic} />
      </Doc>
    );
  }
});
module.exports = IconDoc;
// md文件中，直接使用md语法编写属性介绍说明文档

//测试样例文件02-basic.js
var Instance = (
    <div>
      {*your code*}
    </div>
  );
ReactDOM.render(Instance, mountNode);

```

- **修改样例系统配置文件** 

```
// examples/_pages/PageComponentsDoc.js
/**配置测试组件区域-新增组件后需要在这儿引入API*/
var Docs = {
  close: require('./../close/index'),
  icon:require('./../icon/index'),
  toast:require('./../toast/index')//配置新增的组件测试demo
};
/**配置测试组件区域**/

// examples/ReactBin.js 
/**引入组件模块区域 */
var Close = require('components/close');
var Icon = require('components/icon');
/**引入组件模块区域 */

```

- **启动项目** 
```
npm start
```

- **组件测试完成后进行项目部署** 
```
npm run start:dist   //构建文档管理项目   把dist/www目录copy到 node-Fecar项目中publick/che-react/www中
```
*后期采用Jenkins平台进行部署

### 如何发布到58 npm私服
```
npm run pub   //[详细参考](http://c.58corp.com/pages/viewpage.action?pageId=26157639)
```