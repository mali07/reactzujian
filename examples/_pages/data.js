'use strict';

module.exports = [
  {
    category: 'Common',
    title: '常用组件',
    components: [
      {
        id: 'close',
        name: 'Close',
        title: '关闭按钮'
      },{
        id: 'icon',
        name: 'Icon',
        title: '图标'
      },
      {
        id: 'slider',
        name: 'Slider',
        title: '轮播'
      }
      
    ]
  },
  {
    category: 'mobile',
    title: '移动端组件',
    components: [
      {
        id: 'toast',
        name: 'Toast',
        title: 'toast提示'
      },{
        id: 'dialog',
        name: 'Dialog',
        title: '图标'
      },
      {
        id: 'inputText',
        name: 'InputText',
        title: '输入框'
      },
      {
        id: 'pickerSelector',
        name: 'PickerSelector',
        title: '级联选择器'
      }
    ]
  },
  {
    category: 'hybird',
    title: 'Native/H5混合组件',
    components: [
      {
        id: 'inputText',
        name: 'InputText',
        title: '输入框'
      }
    ]
  }
];
